package com.globus.erpar.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.http.MediaType;

import com.globus.erpar.model.GmARRevenuePDFTrackModel;
import com.globus.erpar.service.GmARRevenuePDFTrackService;

/**
 * This GmARRevenuePDFTrackController class used to fetch/save auto revenue tracking details
 * 
 */

@RequestMapping("/api")
@RestController
public class GmARRevenuePDFTrackController {

	@Autowired
	GmARRevenuePDFTrackService gmARRevenuePDFTrackService;
	
	/**
	 * This method is used to fetch revenue count by aging and category
	 * @param GmARRevenuePDFTrackModel
	 * @return int
	 */
	
	@RequestMapping(value = "fetchRevenueTrackCnt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public int fetchRevenueTrackCnt(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		return gmARRevenuePDFTrackService.fetchRevenueTrackCnt(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to fetch revenue report by filters 
	 * @param GmARRevenuePDFTrackModel
	 * @return GmARRevenueTrackModel List
	 */	
	
	@RequestMapping(value = "fetchRevenueTrackRpt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARRevenuePDFTrackModel> fetchRevenueTrackRpt(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		return gmARRevenuePDFTrackService.fetchRevenueTrackRpt(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to save override PO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@RequestMapping(value = "saveOrverridePORevenueTrack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveOrverridePORevenueTrack(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackService.saveOverridePORevenue(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to save override DO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */
	@RequestMapping(value = "saveOrverrideDORevenueTrack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveOrverrideDORevenueTrack(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackService.saveOverrideDORevenue(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to save comments revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@RequestMapping(value = "saveDoRevenueTrackComments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveDoRevenueTrackComments(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackService.saveDORevenueComment(gmARRevenuePDFTrackModel);
	}
	
	/**
	 * This method is used to  approve revenue track  details
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	@RequestMapping(value = "approveRevenueTrack", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void approveRevenueTrack(@RequestBody GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception {
		gmARRevenuePDFTrackService.approveRevenueTrack(gmARRevenuePDFTrackModel);
	}
}
