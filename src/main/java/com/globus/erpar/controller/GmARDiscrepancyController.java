package com.globus.erpar.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;





import com.globus.erpar.model.GmARDiscrepancyDtlReportModel;
import com.globus.erpar.model.GmARDiscrepancyModel;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;
import com.globus.erpar.service.GmARDiscrepancyService;
import com.globus.erpar.model.GmAROrderPOModel;

/**
 * This GmARDiscrepancyController class used to load/save the Move to Discrepancy Details.. 
 * 
 */


@RequestMapping("/api")
@RestController
public class GmARDiscrepancyController {
	
	@Autowired
	GmARDiscrepancyService gmARDiscrepancyService;
	
    /**
	 * This method is used fetch discrepancy details Report based on filters
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */	
	
	@RequestMapping(value = "loadDiscrepancyDetails",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARDiscrepancyModel> loadDiscrepancyDetails(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception {
		return gmARDiscrepancyService.loadDiscrepancyDetails(gmARDiscrepancyModel);
	}	
	 /**
      * This method is used save/update the discrepancy details(collector,category,resolution)
	  * @param GmARDiscrepancyModel
	  * @return GmARDiscrepancyModel
	  */
	@RequestMapping(value = "savePODiscrepencyDetails",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<GmAROrderPOModel> savePODiscrepencyDetails(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyService.savePODiscrepencyDetails(gmARDiscrepancyModel);
	}
	 /**
	  * This method is used fetch discrepancy details based on PO detail Id(collector,category,resolution,remind me for right panel)
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	@RequestMapping(value = "fetchPODiscrepencyIdList",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<GmAROrderPOModel> fetchPODiscrepencyIdList(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyService.fetchPODiscrepencyIdList(gmARDiscrepancyModel);
	}
	 /**
	  * This method is used to save the Remind me days for discrepancy
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	@RequestMapping(value = "saveRemindMeDiscrepencyDetails",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Optional<GmAROrderPOModel> saveRemindMeDiscrepencyDetails(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyService.saveRemindMeDiscrepencyDetails(gmARDiscrepancyModel);
	}
	 /**
	  * This method is used to fetch the log details for Category,Collector,Resolution(History Icon)
	  * @param GmARDiscrepancyModel
	  * @return GmARDiscrepancyModel
	  */
	@RequestMapping(value = "fetchPOLogDetails",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARDiscrepancyModel> fetchPOLogDetails(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyService.fetchPOLogDetails(gmARDiscrepancyModel);
	}
	
		/**
	 * This method is used to fetch line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	@RequestMapping(value = "loadDiscrepancyLineItems",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARPODiscrepancyLineItemModel> fetchDiscrepancyLineItems(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel)throws Exception {
		return gmARDiscrepancyService.fetchDiscrepancyLineItems(gmARDiscrepancyModel);
	}
	
	/**
	 * This method is used to save line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	@RequestMapping(value = "saveDiscrepencyLineItems",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveDiscrepencyLineItem(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel)throws Exception {
		return gmARDiscrepancyService.saveDiscrepencyLineItems(gmARDiscrepancyModel);
	}
	
	/**
	 * This method is used to remove line item discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	@RequestMapping(value = "removeDiscrepencyLineItems",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void removeDiscrepencyLineItems(@RequestBody GmARDiscrepancyModel gmARDiscrepancyModel)throws Exception {
		gmARDiscrepancyService.removeDiscrepencyLineItems(gmARDiscrepancyModel);
	}
	
   	/**
	 * This method is used to fetch discrepancy details report
	 * @param gmARDiscrepancyDtlReportModel
	 * @return gmARDiscrepancyDtlReportModel
	 */
	@RequestMapping(value = "loadDiscrepancyDtlReport",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GmARDiscrepancyDtlReportModel> loadDiscrepancyDtlReport(@RequestBody GmARDiscrepancyDtlReportModel gmARDiscrepancyDtlReportModel)throws Exception {
		return gmARDiscrepancyService.loadDiscrepancyDtlReport(gmARDiscrepancyDtlReportModel);
	}
	
}
