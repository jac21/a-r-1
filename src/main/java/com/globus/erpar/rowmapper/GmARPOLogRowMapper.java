package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARDiscrepancyModel;


public class GmARPOLogRowMapper implements RowMapper<GmARDiscrepancyModel>{
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARDiscrepancyModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARDiscrepancyModel gmARDiscrepancyModel = new GmARDiscrepancyModel();
		gmARDiscrepancyModel.setStrOrderPOLogDtlId(GmCommonClass.parseNull(resultSet.getString("strLogId")));
		gmARDiscrepancyModel.setStrPODtlCodeNm(GmCommonClass.parseNull(resultSet.getString("strLogCodeNm")));
		gmARDiscrepancyModel.setStrPODtlLogUpdateBy(GmCommonClass.parseNull(resultSet.getString("strLogUpdateBy")));
		gmARDiscrepancyModel.setStrPODtlLogUpdateDate(GmCommonClass.parseNull(resultSet.getString("strLogUpdateDate")));
		return gmARDiscrepancyModel;
	}
}
