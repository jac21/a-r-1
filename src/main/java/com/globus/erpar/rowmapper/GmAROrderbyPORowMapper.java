package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;

public class GmAROrderbyPORowMapper implements RowMapper<GmARCustPOModel> {
	@Autowired
	GmCommonClass gmCommonClass;
	//This is to map the required column from the result set for Associated orders modal pop up body
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("CUSTPO")));
		gmARCustPOModel.setStrCustPOUpdatedBy(GmCommonClass.parseNull(resultSet.getString("CUSTPOUPDBY")));
		gmARCustPOModel.setStrPOEnteredDate(GmCommonClass.parseNull(resultSet.getString("CUSTPODATE")));
		gmARCustPOModel.setStrPOAmt(GmCommonClass.parseNull(resultSet.getString("CUSTPOAMT")));
		return gmARCustPOModel;
	}

}
