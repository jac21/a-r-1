package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARRejectPOEmailModel;


/**
 * This GmARRejectPOEmailRowMapper class used to map reject PO Email details to modal object
 *  
 */

public class GmARRejectPOEmailRowMapper implements RowMapper<GmARRejectPOEmailModel>{
	
	@Override
	public GmARRejectPOEmailModel mapRow(ResultSet resultSet, int i) throws SQLException { 
		GmARRejectPOEmailModel gmARRejectPOEmailModel = new GmARRejectPOEmailModel();
		
		gmARRejectPOEmailModel.setStrPORejectTOEmail(GmCommonClass.parseNull(resultSet.getString("poRejectedTo")));
		gmARRejectPOEmailModel.setStrPORejectAccID(GmCommonClass.parseNull(resultSet.getString("poRejectedAccId")));
		gmARRejectPOEmailModel.setStrPORejectAccNm(GmCommonClass.parseNull(resultSet.getString("poRejectedAccNm")));
		gmARRejectPOEmailModel.setStrPORejectCCEmail(GmCommonClass.parseNull(resultSet.getString("poRejectedCc")));
		gmARRejectPOEmailModel.setStrPORejectFromEmail(GmCommonClass.parseNull(resultSet.getString("poRejectedFrom")));
		gmARRejectPOEmailModel.setStrPORejectSub(GmCommonClass.parseNull(resultSet.getString("poRejectedSubject")));
		gmARRejectPOEmailModel.setStrPORejectConType(GmCommonClass.parseNull(resultSet.getString("poRejectedConType")));
		gmARRejectPOEmailModel.setStrPORejectTitle(GmCommonClass.parseNull(resultSet.getString("poRejectedTitle")));
		gmARRejectPOEmailModel.setStrPORejectBody(GmCommonClass.parseNull(resultSet.getString("poRejectBdy")));
		gmARRejectPOEmailModel.setStrPORejectHeaderFl(GmCommonClass.parseNull(resultSet.getString("poRejectHeaderFl")));	
		return gmARRejectPOEmailModel;
	
	
	

}
}