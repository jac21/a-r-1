package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import jdk.nashorn.internal.ir.SetSplitState;

import com.globus.common.util.GmCommonClass;
import org.springframework.beans.factory.annotation.Autowired;
import com.globus.erpar.model.GmARCustPOModel;

/**
 * This GmARPOStatusRowMapper class used to map the Order Order PO Details . 
 * 
 */

public class GmARPOStatusRowMapper implements RowMapper<GmARCustPOModel> {
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrPONumDtlsId(GmCommonClass.parseNull(resultSet.getString("strPODetaiId")));
		gmARCustPOModel.setStrParentAcc(GmCommonClass.parseNull(resultSet.getString("strPartyName")));
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmARCustPOModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("strCustPO")));
		gmARCustPOModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("strOrderAmt")));
		gmARCustPOModel.setStrPOAmt(GmCommonClass.parseNull(resultSet.getString("strPOAmt")));
		gmARCustPOModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strAccountNm")));
		gmARCustPOModel.setStrFieldSales(GmCommonClass.parseNull(resultSet.getString("strDistNm")));
		gmARCustPOModel.setStrSalesRep(GmCommonClass.parseNull(resultSet.getString("strSalesRepNm")));
		gmARCustPOModel.setStrHoldFl(GmCommonClass.parseNull(resultSet.getString("strHoldFl")));
		gmARCustPOModel.setStrDivisionNm(GmCommonClass.parseNull(resultSet.getString("strDivisionNm")));
		gmARCustPOModel.setStrDOFlag(GmCommonClass.parseNull(resultSet.getString("strUploadFl")));
		gmARCustPOModel.setStrPOStatus(GmCommonClass.parseNull(resultSet.getString("strPOStatus")));
		gmARCustPOModel.setStrDiffAmt(GmCommonClass.parseNull(resultSet.getString("strDiffAmt")));
		gmARCustPOModel.setStrChkHoldFl(GmCommonClass.parseNull(resultSet.getString("strChkHoldfl")));
		gmARCustPOModel.setStrPOEnteredDate(GmCommonClass.parseNull(resultSet.getString("strPOEnteredDt")));
		gmARCustPOModel.setStrParentAccId(GmCommonClass.parseNull(resultSet.getString("strParentAccId")));
		gmARCustPOModel.setStrOrdReceiveMode(GmCommonClass.parseNull(resultSet.getString("strOrdReceiveMode")));
		gmARCustPOModel.setStrPOStatusUpdatedBy(GmCommonClass.parseNull(resultSet.getString("strPOStatusUpdatedBy")));
		gmARCustPOModel.setStrPOStatusUpdatedDate(GmCommonClass.parseNull(resultSet.getString("strPOStatusUpdatedDate")));
		gmARCustPOModel.setStrCustPOUpdatedBy(GmCommonClass.parseNull(resultSet.getString("strPOEnteredBy")));
		return gmARCustPOModel;
	}
	
}
