package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.globus.common.util.GmCommonClass;
import org.springframework.beans.factory.annotation.Autowired;

import com.globus.erpar.model.GmARRevenuePDFTrackModel;

/**
 * This GmARRevenuePDFTrackRowMapper class used to map revenue details into modal object
 *  
 */
public class GmARRevenuePDFTrackRowMapper implements RowMapper<GmARRevenuePDFTrackModel>{
	@Override
	public GmARRevenuePDFTrackModel mapRow(ResultSet resultSet, int i) throws SQLException { 
		GmARRevenuePDFTrackModel gmRevenuePDFTrackModel = new GmARRevenuePDFTrackModel();
		gmRevenuePDFTrackModel.setRevenueTrackId(resultSet.getInt("revenueTrackId"));
		gmRevenuePDFTrackModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("strOrderId")));
		gmRevenuePDFTrackModel.setStrRepAcc(GmCommonClass.parseNull(resultSet.getString("strRepAcc")));
		gmRevenuePDFTrackModel.setStrOrderdate(GmCommonClass.parseNull(resultSet.getString("strOrderdate")));
		gmRevenuePDFTrackModel.setStrCategory(GmCommonClass.parseNull(resultSet.getString("strCategory")));
		gmRevenuePDFTrackModel.setStrAgingNm(GmCommonClass.parseNull(resultSet.getString("strAgingNm")));
		gmRevenuePDFTrackModel.setStrContractFlag(GmCommonClass.parseNull(resultSet.getString("strContractFlag")));
		gmRevenuePDFTrackModel.setStrState(GmCommonClass.parseNull(resultSet.getString("strState")));
		gmRevenuePDFTrackModel.setStrPoRevenue(GmCommonClass.parseNull(resultSet.getString("strPoRevenue")));
		gmRevenuePDFTrackModel.setStrDoRevenue(GmCommonClass.parseNull(resultSet.getString("strDoRevenue")));
		gmRevenuePDFTrackModel.setStrOverideDo(GmCommonClass.parseNull(resultSet.getString("strOverideDo")));
		gmRevenuePDFTrackModel.setStrComments(GmCommonClass.parseNull(resultSet.getString("strComments")));
		gmRevenuePDFTrackModel.setStrOveridePo(GmCommonClass.parseNull(resultSet.getString("strOveridePo")));
		gmRevenuePDFTrackModel.setChkFl(true);
		gmRevenuePDFTrackModel.setStrCustomcss("");
		return gmRevenuePDFTrackModel;
	}
	
}
