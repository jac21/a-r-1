package com.globus.erpar.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;

public class GmARPODiscrepancyLineItemRowMapper implements RowMapper<GmARPODiscrepancyLineItemModel>{
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARPODiscrepancyLineItemModel mapRow(ResultSet resultSet, int i) throws SQLException {
		
		GmARPODiscrepancyLineItemModel gmARPODiscrepancyLineItemModel = new GmARPODiscrepancyLineItemModel();
		gmARPODiscrepancyLineItemModel.setStrOrderid(GmCommonClass.parseNull(resultSet.getString("strOrderid")));
		gmARPODiscrepancyLineItemModel.setStrPnum(GmCommonClass.parseNull(resultSet.getString("strPnumId")));
		gmARPODiscrepancyLineItemModel.setStrItemQty(GmCommonClass.parseNull(resultSet.getString("strItemQty")));
		gmARPODiscrepancyLineItemModel.setStrItemPrice(GmCommonClass.parseNull(resultSet.getString("strItemPrice")));
		gmARPODiscrepancyLineItemModel.setStrPOPartNum(GmCommonClass.parseNull(resultSet.getString("strPOPartNum")));
		gmARPODiscrepancyLineItemModel.setStrPOPartQty(GmCommonClass.parseNull(resultSet.getString("strPOPartQty")));
		gmARPODiscrepancyLineItemModel.setStrPOPrice(GmCommonClass.parseNull(resultSet.getString("strPOPrice")));
		gmARPODiscrepancyLineItemModel.setStrPODtlsId(GmCommonClass.parseNull(resultSet.getString("strPODtlsId")));
		gmARPODiscrepancyLineItemModel.setStrOrdLineItemDtlsId(GmCommonClass.parseNull(resultSet.getString("strPOLIDtlsId")));
		gmARPODiscrepancyLineItemModel.setStrExtDoAmt(GmCommonClass.parseNull(resultSet.getString("strExtDoAmt")));
		gmARPODiscrepancyLineItemModel.setStrExtPoAmt(GmCommonClass.parseNull(resultSet.getString("strExtPoAmt")));
		gmARPODiscrepancyLineItemModel.setStrPartDesc(GmCommonClass.parseNull(resultSet.getString("strPartDesc")));
		gmARPODiscrepancyLineItemModel.setStrDescType(GmCommonClass.parseNull(resultSet.getString("strDescType")));
		gmARPODiscrepancyLineItemModel.setStrStatus(GmCommonClass.parseNull(resultSet.getString("strStatus")));
		gmARPODiscrepancyLineItemModel.setStrDescTypeId(GmCommonClass.parseNull(resultSet.getString("strDescTypeId")));
		gmARPODiscrepancyLineItemModel.setStrStatusId(GmCommonClass.parseNull(resultSet.getString("strStatusId")));
		
		return gmARPODiscrepancyLineItemModel;
	}

}
