package com.globus.erpar.rowmapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.globus.common.util.GmCommonClass;
import org.springframework.beans.factory.annotation.Autowired;
import com.globus.erpar.model.GmARCustPOModel;
/**
 * This GmARCustPORowMapper class used to map the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */

public class GmARCustPORowMapper implements RowMapper<GmARCustPOModel> {
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Override
	public GmARCustPOModel mapRow(ResultSet resultSet, int i) throws SQLException {
		GmARCustPOModel gmARCustPOModel = new GmARCustPOModel();
		gmARCustPOModel.setStrCustPONumber(GmCommonClass.parseNull(resultSet.getString("CUSTPO")));
		gmARCustPOModel.setStrPOAmt(GmCommonClass.parseNull(resultSet.getString("POAMT")));
		gmARCustPOModel.setStrOrderId(GmCommonClass.parseNull(resultSet.getString("ORDID")));
		gmARCustPOModel.setStrPOStatus(GmCommonClass.parseNull(resultSet.getString("POSTATUS")));
		gmARCustPOModel.setStrOrdertype(GmCommonClass.parseNull(resultSet.getString("ORDTYP")));
		gmARCustPOModel.setStrOrderTypeName(GmCommonClass.parseNull(resultSet.getString("ORDTYPENAME")));
		gmARCustPOModel.setStrTotal(GmCommonClass.parseNull(resultSet.getString("TOTAL")));
		gmARCustPOModel.setStrShipCost(GmCommonClass.parseNull(resultSet.getString("SHIPCOST")));
		//gmARCustPOModel.setStrBckOrdCnt(GmCommonClass.parseNull(resultSet.getString(9)));
		gmARCustPOModel.setStrPONumDtlsId(GmCommonClass.parseNull(resultSet.getString("PODTLID"))); 
		gmARCustPOModel.setStrHoldFl(GmCommonClass.parseNull(resultSet.getString("HOLDFL")));
		gmARCustPOModel.setStrBackOrderCnt(GmCommonClass.parseNull(resultSet.getString("BCKORDCNT"))); 
		gmARCustPOModel.setStrDirBckOrdIds(GmCommonClass.parseNull(resultSet.getString("DIRBCKORDIDS")));
		gmARCustPOModel.setStrAccid(GmCommonClass.parseNull(resultSet.getString("strAccId")));
		gmARCustPOModel.setStrOrdReceiveMode(GmCommonClass.parseNull(resultSet.getString("strOrdReceiveMode")));
		return gmARCustPOModel;
	}
}
