package com.globus.erpar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.globus.erpar.dao.GmARDiscrepancyDao;
import com.globus.erpar.model.GmARDiscrepancyModel;

import java.util.Optional;

import com.globus.erpar.model.GmARDiscrepancyDtlReportModel;
import com.globus.erpar.model.GmAROrderPOModel;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;



@Service
public class GmARDiscrepancyServiceImpl implements GmARDiscrepancyService{
	
	@Autowired
	GmARDiscrepancyDao gmARDiscrepancyDao;
	/**
	 * This method is used to fetch discrepancy details Report based on filters
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public List<GmARDiscrepancyModel> loadDiscrepancyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		
		return gmARDiscrepancyDao.loadDiscrepancyDetails(gmARDiscrepancyModel);
	}
	
	/**
	 * This method is used to Save/update discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	public Optional<GmAROrderPOModel> savePODiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyDao.savePODiscrepencyDetails(gmARDiscrepancyModel);
	}
	/**
	 * This method is used fetch discrepancy details based on PO detail Id(collector,category,resolution,remind me for right panel)
	 * @param GmARDiscrepancyModel
	 * @return GmAROrderPOModel
	 */
	public Optional<GmAROrderPOModel> fetchPODiscrepencyIdList(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyDao.fetchPODiscrepencyIdList(gmARDiscrepancyModel);
	}
	/**
	  * This method is used to save the Remind me days for discrepancy
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	public Optional<GmAROrderPOModel> saveRemindMeDiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyDao.saveRemindMeDiscrepencyDetails(gmARDiscrepancyModel);
	}
	/**
	  * This method is used to fetch the log details for Category,Collector,Resolution(History Icon)
	  * @param GmARDiscrepancyModel
	  * @return GmARDiscrepancyModel
	  */
	public List<GmARDiscrepancyModel> fetchPOLogDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyDao.fetchPOLogDetails(gmARDiscrepancyModel);
	}
	
		/**
	 *  This method is used to fetch line item discrepancy details
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public List<GmARPODiscrepancyLineItemModel> fetchDiscrepancyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		
		return gmARDiscrepancyDao.fetchDiscrepancyLineItems(gmARDiscrepancyModel);
	}
	
	
	/**
	 *  This method is used to save line item discrepancy details
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public String saveDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		return gmARDiscrepancyDao.saveDiscrepencyLineItems(gmARDiscrepancyModel);
	}
	
	/**
	 *  This method is used to remove line item discrepancy details
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public void removeDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		gmARDiscrepancyDao.removeDiscrepencyLineItems(gmARDiscrepancyModel);
	}
	
	/**
	 *  This method is used to fetch discrepancy detail report
	 * @param gmARDiscrepancyDtlReportModel
	 * @return gmARDiscrepancyDtlReportModel
	 */
	public List<GmARDiscrepancyDtlReportModel> loadDiscrepancyDtlReport(GmARDiscrepancyDtlReportModel gmARDiscrepancyDtlReportModel) throws Exception{
		return gmARDiscrepancyDao.loadDiscrepancyDtlReport(gmARDiscrepancyDtlReportModel);
	}
	
}
