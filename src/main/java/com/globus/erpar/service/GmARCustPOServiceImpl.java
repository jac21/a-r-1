package com.globus.erpar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.globus.erpar.dao.GmARCustPODao;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.model.GmARRejectPOEmailModel;
/**
 * This GmARCustPOServiceImpl class used to load/save the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */
@Service
public class GmARCustPOServiceImpl implements GmARCustPOService{

	@Autowired
	GmARCustPODao gmARCustPODao;
	/**
	 * This method is used to load the PO report details
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchPendingPODetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		
		return gmARCustPODao.fetchPendingPODetails(gmARCustPOModel);
	}
	/**
	 * This method is used to fetch the Order and PO Details for selected Order
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchOrderDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.fetchOrderDetails(gmARCustPOModel);
	}
	/**
	 * This method is used to fetch the Order Revenue Details for selected Order
	 * @param GmAROrderRevenueModel
	 * @return GmAROrderRevenueModel
	 */
	public GmAROrderRevenueModel  fetchOrderRevenueDetails(GmAROrderRevenueModel gmAROrderRevenueModel) throws Exception{
		return gmARCustPODao.fetchOrderRevenueDetails(gmAROrderRevenueModel);
	};
	/**
	 * This method used to save/update Order Revenue Details for selected Order
	 * @param - GmARCustPOModel
	 * @return - GmAROrderRevenueModel
	 */
	public Optional<GmAROrderRevenueModel> saveOrderRevenueDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.saveOrderRevenueDetails(gmARCustPOModel);
	};
	/**
	 * This method is used to save/update Order PO Details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */	
	public List<GmARCustPOModel> saveOrderPODetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.saveOrderPODetails(gmARCustPOModel);
	}
	/**
	 * This method used to validate the current PO with Existing PO
	 * @param - strPONumber,strCompanyId
	 * @return - Count as String
	 */
	public String validatePO(String strPONumber,String strCompanyId,String strOrderId) throws Exception{
		
			return gmARCustPODao.validatePO(strPONumber,strCompanyId,strOrderId);
		}
	/**
	 * This method is used to fetch the Existing PO Details for entered PO Number
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchPODrilldown(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.fetchPODrilldown(gmARCustPOModel);
	};

	/**
	 * This method is used to save/update PO Status(Ready For Invoicing,Move to Discrepancy,Reject)
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public void saveOrderPOStatus(GmARCustPOModel gmARCustPOModel) throws Exception{
		gmARCustPODao.saveOrderPOStatus(gmARCustPOModel);
	}
	
	/**
	   * Description: This method is used to get report PO status Report
	   * Author
	   * @param GmARCustPOModel
	   * @return List<GmARCustPOModel>
	   * @exception Exception 
	   **/
	public List<GmARCustPOModel> fetchReadyForInvoiceDtls(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.fetchReadyForInvoiceDtls(gmARCustPOModel);
	};
	
	
	/**
	   * This method used to save/update PO's discrepancy Details for selected PO
	   * Author
	   * @param GmARCustPOModel
	   * @exception Exception 
	   **/
	public void saveDiscrepancyDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		gmARCustPODao.saveDiscrepancyDetails(gmARCustPOModel);
	};
	
	/**
	   * This method used to fetch PO Format drill down details based on account
	   * Author
	   * @param GmARCustPOModel
	   * @exception Exception 
	   **/
	public List<GmARCustPOModel> fetchPOFormatDrilldownDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		return gmARCustPODao.fetchPOFormatDrilldownDetails(gmARCustPOModel);
	}
	
	/**
	   * This method used to fetch Reject PO Notify Email details
	   * Author
	   * @param GmARCustPOModel
	   * @exception Exception 
	   **/
	public List<GmARRejectPOEmailModel> fetchRejectPONotifyEmailDtls(GmARRejectPOEmailModel gmARRejectPOEmailModel) throws Exception{
		return gmARCustPODao.fetchRejectPONotifyEmailDtls(gmARRejectPOEmailModel);
	}
	
		/**
	   * This method used to move PO's into Processor Error for selected PO
	   * Author
	   * @param GmARCustPOModel
	   * @exception Exception 
	   **/
	public void saveProcessorErrorDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		gmARCustPODao.saveProcessorErrorDetails(gmARCustPOModel);
	};

	
}
