package com.globus.erpar.service;

import com.globus.erpar.model.GmARRevenuePDFTrackModel;
import org.springframework.stereotype.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * This GmARRevenuePDFTrackService class used to fetch/save auto revenue tracking details
 * Service interface 
 */
public interface GmARRevenuePDFTrackService {
	
	/**
	 * This method is used to fetch revenue count by aging and category
	 * @param GmARRevenuePDFTrackModel
	 * @return int
	 */
	public int fetchRevenueTrackCnt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;
	
	/**
	 * This method is used to fetch revenue report by filters 
	 * @param GmARRevenuePDFTrackModel
	 * @return GmARRevenueTrackModel List
	 */	
	public List<GmARRevenuePDFTrackModel> fetchRevenueTrackRpt(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;
	
	/**
	 * This method is used to save override PO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	public void saveOverridePORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;
	
	/**
	 * This method is used to save DO revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	public void saveOverrideDORevenue(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;
	
	/**
	 * This method is used to save comments revenue track  
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	public void saveDORevenueComment(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;
	
	/**
	 * This method is used to  approve revenue track  details
	 * @param GmARRevenuePDFTrackModel
	 * @return void
	 */	
	public void approveRevenueTrack(GmARRevenuePDFTrackModel gmARRevenuePDFTrackModel) throws Exception;

}
