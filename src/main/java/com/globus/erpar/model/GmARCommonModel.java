package com.globus.erpar.model;

/**
 * This GmARCommonModel class used to get and set the Common values for Order Revenue Details,Order PO Details and PO Status. 
 * 
 */
public class GmARCommonModel {
	private String strCompanyId;
	private String strCurrency;
	private String strDtFormat;
	private String strDateType;
	private String strDivisionId;
	private String strEndDate;
	private String strFieldSales;
	private String strOrdertype;
	private String strParentAcc;
	private String strRepAcc;
	private String strSalesRep;
	private String strStartDate;
	private String strADName;
	private String strVPName;
	private String strSurgeryDt;
	private String strParentOrdId;
	private String strAccid;
	private String strUserId;
	private String strCmpCurrFmt;
	private String strUserName;
	private String strDivisionNm;
	private String strParentAccId;
	private String strCompZone;
	private String strPlantId;



	
	public String getStrParentAccId() {
		return strParentAccId;
	}
	public void setStrParentAccId(String strParentAccId) {
		this.strParentAccId = strParentAccId;
	}
	public String getStrCompanyId() {
		return strCompanyId;
	}
	public void setStrCompanyId(String strCompanyId) {
		this.strCompanyId = strCompanyId;
	}
	public String getStrCurrency() {
		return strCurrency;
	}
	public void setStrCurrency(String strCurrency) {
		this.strCurrency = strCurrency;
	}
	public String getStrDtFormat() {
		return strDtFormat;
	}
	public void setStrDtFormat(String strDtFormat) {
		this.strDtFormat = strDtFormat;
	}
	public String getStrDateType() {
		return strDateType;
	}
	public void setStrDateType(String strDateType) {
		this.strDateType = strDateType;
	}
	public String getStrDivisionId() {
		return strDivisionId;
	}
	public void setStrDivisionId(String strDivisionId) {
		this.strDivisionId = strDivisionId;
	}
	public String getStrEndDate() {
		return strEndDate;
	}
	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	public String getStrFieldSales() {
		return strFieldSales;
	}
	public void setStrFieldSales(String strFieldSales) {
		this.strFieldSales = strFieldSales;
	}
	public String getStrOrdertype() {
		return strOrdertype;
	}
	public void setStrOrdertype(String strOrdertype) {
		this.strOrdertype = strOrdertype;
	}
	public String getStrParentAcc() {
		return strParentAcc;
	}
	public void setStrParentAcc(String strParentAcc) {
		this.strParentAcc = strParentAcc;
	}
	public String getStrRepAcc() {
		return strRepAcc;
	}
	public void setStrRepAcc(String strRepAcc) {
		this.strRepAcc = strRepAcc;
	}
	public String getStrSalesRep() {
		return strSalesRep;
	}
	public void setStrSalesRep(String strSalesRep) {
		this.strSalesRep = strSalesRep;
	}
	public String getStrStartDate() {
		return strStartDate;
	}
	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}
	public String getStrADName() {
		return strADName;
	}
	public void setStrADName(String strADName) {
		this.strADName = strADName;
	}
	public String getStrVPName() {
		return strVPName;
	}
	public void setStrVPName(String strVPName) {
		this.strVPName = strVPName;
	}
	public String getStrSurgeryDt() {
		return strSurgeryDt;
	}
	public void setStrSurgeryDt(String strSurgeryDt) {
		this.strSurgeryDt = strSurgeryDt;
	}
	public String getStrParentOrdId() {
		return strParentOrdId;
	}
	public void setStrParentOrdId(String strParentOrdId) {
		this.strParentOrdId = strParentOrdId;
	}
	public String getStrAccid() {
		return strAccid;
	}
	public void setStrAccid(String strAccid) {
		this.strAccid = strAccid;
	}
	public String getStrUserId() {
		return strUserId;
	}
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	
	public String getStrCmpCurrFmt() {
		return strCmpCurrFmt;
	}
	public void setStrCmpCurrFmt(String strCmpCurrFmt) {
		this.strCmpCurrFmt = strCmpCurrFmt;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrDivisionNm() {
		return strDivisionNm;
	}
	public void setStrDivisionNm(String strDivisionNm) {
		this.strDivisionNm = strDivisionNm;
	}
	public String getStrCompZone() {
		return strCompZone;
	}
	public void setStrCompZone(String strCompZone) {
		this.strCompZone = strCompZone;
	}
	public String getStrPlantId() {
		return strPlantId;
	}
	public void setStrPlantId(String strPlantId) {
		this.strPlantId = strPlantId;
	}
	
}
