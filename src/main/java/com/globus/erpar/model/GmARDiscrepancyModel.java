package com.globus.erpar.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * This GmARDiscrepancyModel class used to get and set the values for
 * Discrepancy details of PO.
 * 
 */

public class GmARDiscrepancyModel extends GmARCustPOModel {

	private String strCategoryId;
	private String strCategoryNm;
	private String strResolutionId;
	private String strCollectorId;
	private String strReadyforRes;
	private String strHideFutRem;
	private String strDisStartDate;
	private String strDisEndDate;
	private String strResStartDate;
	private String strResEndDate;
	private String strResDate;
	private String strDisdate;
	private String strCollectorNm;
	private String strRemindMedays;
	private Date strRemindMedate;
	private int strPODtlHisType;
	private String strADId;
	private String strVPId;
    private String strDefaultCollector;
	private String strOrderPOLogDtlId;
	private String strPODtlCodeNm;
	private String strPODtlLogUpdateBy;
	private String strPODtlLogUpdateDate;
	private String strPONumber;
	private String strAccId;
	private String strPODtlsId;
	private String strPOPartNum;
	//Po line item json variable	
	private String strPOLineItemDtls;
	private String strPnumInputStr;
	
	
	// for po resolved status
	private String strPOResolvefl;
	private String strPODiscRemdate;
	private String strReadyForResId;
	private String strHFutureRemId;
	
	public String getStrReadyForResId() {
		return strReadyForResId;
	}
	public void setStrReadyForResId(String strReadyForResId) {
		this.strReadyForResId = strReadyForResId;
	}
	public String getStrHFutureRemId() {
		return strHFutureRemId;
	}
	public void setStrHFutureRemId(String strHFutureRemId) {
		this.strHFutureRemId = strHFutureRemId;
	}
	
	/**
	 * @return the strPODiscRemdate
	 */
	public String getStrPODiscRemdate() {
		return strPODiscRemdate;
	}
	/**
	 * @param strPODiscRemdate
	 * the strPODiscRemdate to set
	 */
	public void setStrPODiscRemdate(String strPODiscRemdate) {
		this.strPODiscRemdate = strPODiscRemdate;
	}
	/**
	 * @return the strPOResolvefl
	 */
	public String getStrPOResolvefl() {
		return strPOResolvefl;
	}
	/**
	 * @param strPOResolvefl
	 * the strPOResolvefl to set
	 */
	public void setStrPOResolvefl(String strPOResolvefl) {
		this.strPOResolvefl = strPOResolvefl;
	}
	

	
	/**
	 * @return the StrADId
	 */
	public String getStrADId() {
		return strADId;
	}
	/**
	 * @param StrADId
	 *            the StrADId to set
	 */
	public void setStrADId(String strADId) {
		this.strADId = strADId;
	}
	/**
	 * @return the StrVPId
	 */
	public String getStrVPId() {
		return strVPId;
	}
	/**
	 * @param StrVPId
	 *            the StrVPId to set
	 */
	public void setStrVPId(String strVPId) {
		this.strVPId = strVPId;
	}
		/**
	 * @return the strDefaultCollector
	 */
	public String getStrDefaultCollector() {
		return strDefaultCollector;
	}
	/**
	 * @param strDefaultCollector
	 *            the strDefaultCollector to set
	 */
	public void setStrDefaultCollector(String strDefaultCollector) {
		this.strDefaultCollector = strDefaultCollector;
	}

	/**
	 * @return the strCollectorNm
	 */
	public String getStrCollectorNm() {
		return strCollectorNm;
	}

	/**
	 * @param strCollectorNm
	 *            the strCollectorNm to set
	 */
	public void setStrCollectorNm(String strCollectorNm) {
		this.strCollectorNm = strCollectorNm;
	}

	/**
	 * @return the strResDate
	 */
	public String getStrResDate() {
		return strResDate;
	}

	/**
	 * @param strResDate
	 *            the strResDate to set
	 */
	public void setStrResDate(String strResDate) {
		this.strResDate = strResDate;
	}

	/**
	 * @return the strDisdate
	 */
	public String getStrDisdate() {
		return strDisdate;
	}

	/**
	 * @param strDisdate
	 *            the strDisdate to set
	 */
	public void setStrDisdate(String strDisdate) {
		this.strDisdate = strDisdate;
	}

	/**
	 * @return the strCategoryId
	 */
	public String getStrCategoryId() {
		return strCategoryId;
	}

	/**
	 * @param strCategoryId
	 *            the strCategoryId to set
	 */
	public void setStrCategoryId(String strCategoryId) {
		this.strCategoryId = strCategoryId;
	}

	/**
	 * @return the strCategoryNm
	 */
	public String getStrCategoryNm() {
		return strCategoryNm;
	}

	/**
	 * @param strCategoryNm
	 *            the strCategoryNm to set
	 */
	public void setStrCategoryNm(String strCategoryNm) {
		this.strCategoryNm = strCategoryNm;
	}

	/**
	 * @return the strResolutionId
	 */
	public String getStrResolutionId() {
		return strResolutionId;
	}

	/**
	 * @param strResolutionId
	 *            the strResolutionId to set
	 */
	public void setStrResolutionId(String strResolutionId) {
		this.strResolutionId = strResolutionId;
	}

	/**
	 * @return the strCollectorId
	 */
	public String getStrCollectorId() {
		return strCollectorId;
	}

	/**
	 * @param strCollectorId
	 *            the strCollectorId to set
	 */
	public void setStrCollectorId(String strCollectorId) {
		this.strCollectorId = strCollectorId;
	}

	/**
	 * @return the strReadyforRes
	 */
	public String getStrReadyforRes() {
		return strReadyforRes;
	}

	/**
	 * @param strReadyforRes
	 *            the strReadyforRes to set
	 */
	public void setStrReadyforRes(String strReadyforRes) {
		this.strReadyforRes = strReadyforRes;
	}

	/**
	 * @return the strHideFutRem
	 */
	public String getStrHideFutRem() {
		return strHideFutRem;
	}

	/**
	 * @param strHideFutRem
	 *            the strHideFutRem to set
	 */
	public void setStrHideFutRem(String strHideFutRem) {
		this.strHideFutRem = strHideFutRem;
	}

	/**
	 * @return the strDisStartDate
	 */
	public String getStrDisStartDate() {
		return strDisStartDate;
	}

	/**
	 * @param strDisStartDate
	 *            the strDisStartDate to set
	 */
	public void setStrDisStartDate(String strDisStartDate) {
		this.strDisStartDate = strDisStartDate;
	}

	/**
	 * @return the strDisEndDate
	 */
	public String getStrDisEndDate() {
		return strDisEndDate;
	}

	/**
	 * @param strDisEndDate
	 *            the strDisEndDate to set
	 */
	public void setStrDisEndDate(String strDisEndDate) {
		this.strDisEndDate = strDisEndDate;
	}

	/**
	 * @return the strResStartDate
	 */
	public String getStrResStartDate() {
		return strResStartDate;
	}

	/**
	 * @param strResStartDate
	 *            the strResStartDate to set
	 */
	public void setStrResStartDate(String strResStartDate) {
		this.strResStartDate = strResStartDate;
	}

	/**
	 * @return the strResEndDate
	 */
	public String getStrResEndDate() {
		return strResEndDate;
	}

	/**
	 * @param strResEndDate
	 *            the strResEndDate to set
	 */
	public void setStrResEndDate(String strResEndDate) {
		this.strResEndDate = strResEndDate;
	}

	/**
	 * @return the strRemindMedays
	 */
	public String getStrRemindMedays() {
		return strRemindMedays;
	}

	/**
	 * @param strRemindMedays
	 *            the strRemindMedays to set
	 */
	public void setStrRemindMedays(String strRemindMedays) {
		this.strRemindMedays = strRemindMedays;
	}

	/**
	 * @return the strRemindMedate
	 */
	public Date getStrRemindMedate() {
		return strRemindMedate;
	}

	/**
	 * @param strRemindMedate
	 *            the strRemindMedate to set
	 */
	public void setStrRemindMedate(Date strRemindMedate) {
		this.strRemindMedate = strRemindMedate;
	}

	/**
	 * @return the strPODtlHisType
	 */
	public int getStrPODtlHisType() {
		return strPODtlHisType;
	}

	/**
	 * @param strPODtlHisType
	 *            the strPODtlHisType to set
	 */
	public void setStrPODtlHisType(int strPODtlHisType) {
		this.strPODtlHisType = strPODtlHisType;
	}
	/**
	 * @return the strOrderPOLogDtlId
	 */
	public String getStrOrderPOLogDtlId() {
		return strOrderPOLogDtlId;
	}
	/**
	 * @param strOrderPOLogDtlId
	 *            the strOrderPOLogDtlId to set
	 */
	public void setStrOrderPOLogDtlId(String strOrderPOLogDtlId) {
		this.strOrderPOLogDtlId = strOrderPOLogDtlId;
	}
	/**
	 * @return the strPODtlCodeNm
	 */
	public String getStrPODtlCodeNm() {
		return strPODtlCodeNm;
	}
	/**
	 * @param strPODtlCodeNm
	 *            the strPODtlCodeNm to set
	 */
	public void setStrPODtlCodeNm(String strPODtlCodeNm) {
		this.strPODtlCodeNm = strPODtlCodeNm;
	}
	/**
	 * @return the strPODtlLogUpdateBy
	 */
	public String getStrPODtlLogUpdateBy() {
		return strPODtlLogUpdateBy;
	}
	/**
	 * @param strPODtlLogUpdateBy
	 *            the strPODtlLogUpdateBy to set
	 */
	public void setStrPODtlLogUpdateBy(String strPODtlLogUpdateBy) {
		this.strPODtlLogUpdateBy = strPODtlLogUpdateBy;
	}
	/**
	 * @return the strPODtlLogUpdateDate
	 */
	public String getStrPODtlLogUpdateDate() {
		return strPODtlLogUpdateDate;
	}
	/**
	 * @param strPODtlLogUpdateDate
	 *            the strPODtlLogUpdateDate to set
	 */

	public void setStrPODtlLogUpdateDate(String strPODtlLogUpdateDate) {
		this.strPODtlLogUpdateDate = strPODtlLogUpdateDate;
	}
	
	public String getStrPOPartNum() {
		return strPOPartNum;
	}
	/**
	 * @param strPOPartNum
	 *            the strPOPartNum to set
	 */
	public void setStrPOPartNum(String strPOPartNum) {
		this.strPOPartNum = strPOPartNum;
	}
	public String getStrPONumber() {
		return strPONumber;
	}
	/**
	 * @param strPONumber
	 *            the strPONumber to set
	 */
	public void setStrPONumber(String strPONumber) {
		this.strPONumber = strPONumber;
	}
	public String getStrAccId() {
		return strAccId;
	}
	/**
	 * @param strAccId
	 *            the strAccId to set
	 */
	public void setStrAccId(String strAccId) {
		this.strAccId = strAccId;
	}
	
	
	public String getStrPOLineItemDtls() {
		return strPOLineItemDtls;
	}
	/**
	 * @param strPOLineItemDtls
	 *            the strPOLineItemDtls to set
	 */
	public void setStrPOLineItemDtls(String strPOLineItemDtls) {
		this.strPOLineItemDtls = strPOLineItemDtls;
	}
	public String getStrPODtlsId() {
		return strPODtlsId;
	}
	/**
	 * @param strPODtlsId
	 *            the strPODtlsId to set
	 */
	public void setStrPODtlsId(String strPODtlsId) {
		this.strPODtlsId = strPODtlsId;
	}
	
	public String getStrPnumInputStr() {
		return strPnumInputStr;
	}
	/**
	 * @param strPnumInputStr
	 *            the strPnumInputStr to set
	 */
	public void setStrPnumInputStr(String strPnumInputStr) {
		this.strPnumInputStr = strPnumInputStr;
	}
	

}
