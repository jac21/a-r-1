package com.globus.erpar.model;

import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * This GmAROrderRevenueModel class used to get and set the values for Order Revenue Detail
 * 
 */
@Entity
@Table(name="t5003_order_revenue_sample_dtls")

public class GmAROrderRevenueModel extends GmARCommonModel {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( name = "C5003_ORDER_REVENUE_SAMPLE_DTLS_ID")
	private int strOrdRevId;
	@Column ( name = "C501_ORDER_ID")
	private String strOrderId;
	@Column ( name = "C901_DO_DTLS")
	private String strDODtls;
	@Column ( name = "C901_PO_DTLS")
	private String strPODtls;
	@Column ( name = "C5003_DO_HISTORY_FL")
	private String strDOHistFL;
	@Column ( name = "C5003_PO_HISTORY_FL")
	private String strPOHistFL;
	@Column ( name = "C5003_DO_CREATED_BY")
	private String strDOCreatedBy;
	@Column ( name = "C5003_DO_CREATED_DATE")
	private Date strDOCreatedDate;
	@Column ( name = "C5003_PO_CREATED_BY")
	private String strPOCreatedBy;
	@Column ( name = "C5003_PO_CREATED_DATE")
	private Date strPOCreatedDate;
	@Column ( name = "C5003_DO_LAST_UPDATED_BY")
	private String strDOLastUpdatedBy;
	@Column ( name = "C5003_DO_LAST_UPDATED_DATE")
	private Date strDOLastUpdatedDate;
	@Column ( name = "C5003_PO_LAST_UPDATED_BY")
	private String strPOLastUpdatedBy;
	@Column ( name = "C5003_PO_LAST_UPDATED_DATE")
	private Date strPOLastUpdatedDate;
	@Column ( name = "C5003_VOID_FL")
	private String strVoidFL;
	@Column ( name = "C5003_UPDATED_BY")
	private String strUpdatedBy;
	@Column ( name = "C5003_UPDATED_DATE")
	private String strUpdatedDate;
	
	public int getStrOrdRevId() {
		return strOrdRevId;
	}
	public void setStrOrdRevId(int strOrdRevId) {
		this.strOrdRevId = strOrdRevId;
	}
	public String getStrOrderId() {
		return strOrderId;
	}
	public void setStrOrderId(String strOrderId) {
		this.strOrderId = strOrderId;
	}
	public String getStrDODtls() {
		return strDODtls;
	}
	public void setStrDODtls(String strDODtls) {
		this.strDODtls = strDODtls;
	}
	public String getStrPODtls() {
		return strPODtls;
	}
	public void setStrPODtls(String strPODtls) {
		this.strPODtls = strPODtls;
	}
	public String getStrDOHistFL() {
		return strDOHistFL;
	}
	public void setStrDOHistFL(String strDOHistFL) {
		this.strDOHistFL = strDOHistFL;
	}
	public String getStrPOHistFL() {
		return strPOHistFL;
	}
	public void setStrPOHistFL(String strPOHistFL) {
		this.strPOHistFL = strPOHistFL;
	}
	public String getStrDOCreatedBy() {
		return strDOCreatedBy;
	}
	public void setStrDOCreatedBy(String strDOCreatedBy) {
		this.strDOCreatedBy = strDOCreatedBy;
	}
	public Date getStrDOCreatedDate() {
		return strDOCreatedDate;
	}
	public void setStrDOCreatedDate(Date strDOCreatedDate) {
		this.strDOCreatedDate = strDOCreatedDate;
	}
	public String getStrPOCreatedBy() {
		return strPOCreatedBy;
	}
	public void setStrPOCreatedBy(String strPOCreatedBy) {
		this.strPOCreatedBy = strPOCreatedBy;
	}
	public Date getStrPOCreatedDate() {
		return strPOCreatedDate;
	}
	public void setStrPOCreatedDate(Date strPOCreatedDate) {
		this.strPOCreatedDate = strPOCreatedDate;
	}
	public String getStrDOLastUpdatedBy() {
		return strDOLastUpdatedBy;
	}
	public void setStrDOLastUpdatedBy(String strDOLastUpdatedBy) {
		this.strDOLastUpdatedBy = strDOLastUpdatedBy;
	}
	
	public Date getStrDOLastUpdatedDate() {
		return strDOLastUpdatedDate;
	}
	public void setStrDOLastUpdatedDate(Date strDOLastUpdatedDate) {
		this.strDOLastUpdatedDate = strDOLastUpdatedDate;
	}
	public String getStrPOLastUpdatedBy() {
		return strPOLastUpdatedBy;
	}
	public void setStrPOLastUpdatedBy(String strPOLastUpdatedBy) {
		this.strPOLastUpdatedBy = strPOLastUpdatedBy;
	}
	
	public Date getStrPOLastUpdatedDate() {
		return strPOLastUpdatedDate;
	}
	public void setStrPOLastUpdatedDate(Date strPOLastUpdatedDate) {
		this.strPOLastUpdatedDate = strPOLastUpdatedDate;
	}
	public String getStrVoidFL() {
		return strVoidFL;
	}
	public void setStrVoidFL(String strVoidFL) {
		this.strVoidFL = strVoidFL;
	}
	public String getStrUpdatedBy() {
		return strUpdatedBy;
	}
	public void setStrUpdatedBy(String strUpdatedBy) {
		this.strUpdatedBy = strUpdatedBy;
	}
	public String getStrUpdatedDate() {
		return strUpdatedDate;
	}
	public void setStrUpdatedDate(String strUpdatedDate) {
		this.strUpdatedDate = strUpdatedDate;
	}
	
	
}
