package com.globus.erpar.model;

import java.util.Date;

/**
 * This GmARCustPOModel class used to get and set the values for Order Revenue Details,Order PO Details and PO Status. 
 * 
 */

public class GmARCustPOModel extends GmARCommonModel{
	
	private String strTotal;
	private String strOrderId;
	private String strAccid;
	private String strOrdDT;
	private String strOrdStartDate;
	private String strOrdEndDate;
	private String strHoldFl;
	private String strParentOrdId;
	private String strShipDt;
	private String strShipTrack;
	private String strCustPONumber;
	private String strInvoice;
	private String strPOAmt;
	private String strPODtls;
	private String strDODtls;
	private String strOrderTypeName;
	private String strShipCost;
	private String strPONumDtlsId;
	private String strPOStatus;
	private Date strDOLastUpdatedDate;
	private String strBackOrderCnt;
	private String strDirBckOrdIds;
	private String strLanguageId;
	private String strOrdReceiveMode;
	private String strPONumber;
	private String strPOStatusList;
	private String strDOFlag;
	private String strDiffAmt;
	private String strOrderPODtlIdList;
	private String strUserId;
	private String strChkHoldFl;
	private String strParentAccId;
	private String strPOEnteredFromDate;
	private String strPOEnteredToDate;
	private String strPOEnteredDate;
	
	private String strPOStatusUpdatedBy;
	private String strPOStatusUpdatedDate;
	private String strCustPOUpdatedBy;
	
	
	//PO Format Details
   private String strInvoiceDt;
   private String strInvoiceAmt;
   private String strPORdyForInvDate;

	
	/**
	 * @return the strPOStatusUpdatedBy
	 */
	public String getStrPOStatusUpdatedBy() {
		return strPOStatusUpdatedBy;
	}
	/**
	 * @param strPOStatusUpdatedBy the strPOStatusUpdatedBy to set
	 */
	public void setStrPOStatusUpdatedBy(String strPOStatusUpdatedBy) {
		this.strPOStatusUpdatedBy = strPOStatusUpdatedBy;
	}
	/**
	 * @return the strPOStatusUpdatedDate
	 */
	public String getStrPOStatusUpdatedDate() {
		return strPOStatusUpdatedDate;
	}
	/**
	 * @param strPOStatusUpdatedDate the strPOStatusUpdatedDate to set
	 */
	public void setStrPOStatusUpdatedDate(String strPOStatusUpdatedDate) {
		this.strPOStatusUpdatedDate = strPOStatusUpdatedDate;
	}
   public String getStrPORdyForInvDate() {
	   return strPORdyForInvDate;
   }
   public void setStrPORdyForInvDate(String strPORdyForInvDate) {
	   this.strPORdyForInvDate = strPORdyForInvDate;
   }
	public String getStrCustPOUpdatedBy() {
		return strCustPOUpdatedBy;
	}
	public void setStrCustPOUpdatedBy(String strCustPOUpdatedBy) {
		this.strCustPOUpdatedBy = strCustPOUpdatedBy;
	}
		
	
	public String getStrTotal() {
		return strTotal;
	}
	public void setStrTotal(String strTotal) {
		this.strTotal = strTotal;
	}
	
	public String getStrOrderId() {
		return strOrderId;
	}
	public void setStrOrderId(String strOrderId) {
		this.strOrderId = strOrderId;
	}
	public String getStrAccid() {
		return strAccid;
	}
	public void setStrAccid(String strAccid) {
		this.strAccid = strAccid;
	}
	public String getStrOrdDT() {
		return strOrdDT;
	}
	public void setStrOrdDT(String strOrdDT) {
		this.strOrdDT = strOrdDT;
	}
	public String getStrOrdStartDate() {
		return strOrdStartDate;
	}
	public void setStrOrdStartDate(String strOrdStartDate) {
		this.strOrdStartDate = strOrdStartDate;
	}
	public String getStrOrdEndDate() {
		return strOrdEndDate;
	}
	public void setStrOrdEndDate(String strOrdEndDate) {
		this.strOrdEndDate = strOrdEndDate;
	}
	public String getStrHoldFl() {
		return strHoldFl;
	}
	public void setStrHoldFl(String strHoldFl) {
		this.strHoldFl = strHoldFl;
	}
	
	public String getStrParentOrdId() {
		return strParentOrdId;
	}
	public void setStrParentOrdId(String strParentOrdId) {
		this.strParentOrdId = strParentOrdId;
	}
	public String getStrShipDt() {
		return strShipDt;
	}
	public void setStrShipDt(String strShipDt) {
		this.strShipDt = strShipDt;
	}
	public String getStrShipTrack() {
		return strShipTrack;
	}
	public void setStrShipTrack(String strShipTrack) {
		this.strShipTrack = strShipTrack;
	}
	
	public String getStrCustPONumber() {
		return strCustPONumber;
	}
	public void setStrCustPONumber(String strCustPONumber) {
		this.strCustPONumber = strCustPONumber;
	}
	public String getStrInvoice() {
		return strInvoice;
	}
	public void setStrInvoice(String strInvoice) {
		this.strInvoice = strInvoice;
	}
	public String getStrPOAmt() {
		return strPOAmt;
	}
	public void setStrPOAmt(String strPOAmt) {
		this.strPOAmt = strPOAmt;
	}
	
	
	public String getStrOrderTypeName() {
		return strOrderTypeName;
	}
	public void setStrOrderTypeName(String strOrderTypeName) {
		this.strOrderTypeName = strOrderTypeName;
	}
	public String getStrShipCost() {
		return strShipCost;
	}
	public void setStrShipCost(String strShipCost) {
		this.strShipCost = strShipCost;
	}
	public String getStrPONumDtlsId() {
		return strPONumDtlsId;
	}
	public void setStrPONumDtlsId(String strPONumDtlsId) {
		this.strPONumDtlsId = strPONumDtlsId;
	}
	public String getStrPOStatus() {
		return strPOStatus;
	}
	public void setStrPOStatus(String strPOStatus) {
		this.strPOStatus = strPOStatus;
	}
	public Date getStrDOLastUpdatedDate() {
		return strDOLastUpdatedDate;
	}
	public void setStrDOLastUpdatedDate(Date strDOLastUpdatedDate) {
		this.strDOLastUpdatedDate = strDOLastUpdatedDate;
	}
	public String getStrBackOrderCnt() {
		return strBackOrderCnt;
	}
	public void setStrBackOrderCnt(String strBackOrderCnt) {
		this.strBackOrderCnt = strBackOrderCnt;
	}
	public String getStrDirBckOrdIds() {
		return strDirBckOrdIds;
	}
	public void setStrDirBckOrdIds(String strDirBckOrdIds) {
		this.strDirBckOrdIds = strDirBckOrdIds;
	}
	public String getStrPODtls() {
		return strPODtls;
	}
	public void setStrPODtls(String strPODtls) {
		this.strPODtls = strPODtls;
	}
	public String getStrDODtls() {
		return strDODtls;
	}
	public void setStrDODtls(String strDODtls) {
		this.strDODtls = strDODtls;
	}
	public String getStrLanguageId() {
		return strLanguageId;
	}
	public void setStrLanguageId(String strLanguageId) {
		this.strLanguageId = strLanguageId;
	}
	
	
	public String getStrOrdReceiveMode() {
		return strOrdReceiveMode;
	}
	public void setStrOrdReceiveMode(String strOrdReceiveMode) {
		this.strOrdReceiveMode = strOrdReceiveMode;
	}
	public String getStrPONumber() {
		return strPONumber;
	}
	public void setStrPONumber(String strPONumber) {
		this.strPONumber = strPONumber;
	}
	
	public String getStrPOStatusList() {
		return strPOStatusList;
	}
	public void setStrPOStatusList(String strPOStatusList) {
		this.strPOStatusList = strPOStatusList;
	}
	
	public String getStrDOFlag() {
		return strDOFlag;
	}
	public void setStrDOFlag(String strDOFlag) {
		this.strDOFlag = strDOFlag;
	}
	
	public String getStrDiffAmt() {
		return strDiffAmt;
	}
	public void setStrDiffAmt(String strDiffAmt) {
		this.strDiffAmt = strDiffAmt;
	}
	public String getStrOrderPODtlIdList() {
		return strOrderPODtlIdList;
	}
	public void setStrOrderPODtlIdList(String strOrderPODtlIdList) {
		this.strOrderPODtlIdList = strOrderPODtlIdList;
	}
	public String getStrUserId() {
		return strUserId;
	}
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	
	public String getStrChkHoldFl() {
		return strChkHoldFl;
	}
	public void setStrChkHoldFl(String strChkHoldFl) {
		this.strChkHoldFl = strChkHoldFl;
	}
    public String getStrParentAccId() {
		return strParentAccId;
	}
	public void setStrParentAccId(String strParentAccId) {
		this.strParentAccId = strParentAccId;
	}
	
		public String getStrPOEnteredFromDate() {
		return strPOEnteredFromDate;
	}
	public void setStrPOEnteredFromDate(String strPOEnteredFromDate) {
		this.strPOEnteredFromDate = strPOEnteredFromDate;
	}
	public String getStrPOEnteredToDate() {
		return strPOEnteredToDate;
	}
	public void setStrPOEnteredToDate(String strPOEnteredToDate) {
		this.strPOEnteredToDate = strPOEnteredToDate;
	}

	public String getStrPOEnteredDate() {
		return strPOEnteredDate;
	}
	public void setStrPOEnteredDate(String strPOEnteredDate) {
		this.strPOEnteredDate = strPOEnteredDate;
	}
	public String getStrInvoiceDt() {
		return strInvoiceDt;
	}
	public void setStrInvoiceDt(String strInvoiceDt) {
		this.strInvoiceDt = strInvoiceDt;
	}
	public String getStrInvoiceAmt() {
		return strInvoiceAmt;
	}
	public void setStrInvoiceAmt(String strInvoiceAmt) {
		this.strInvoiceAmt = strInvoiceAmt;
	}
}
