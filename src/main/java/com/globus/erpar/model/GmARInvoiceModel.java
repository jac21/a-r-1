package com.globus.erpar.model;

import java.util.Date;

public class GmARInvoiceModel extends GmARCommonModel{
	
	private String strCompanyId;
	private String strUserId;
	private String inputStr;
	private String strMessage;
	private String strCompanyInfo;
	
	/**
	 * @return the strCompanyId
	 */
	public String getStrCompanyId() {
		return strCompanyId;
	}
	/**
	 * @param strCompanyId the strCompanyId to set
	 */
	public void setStrCompanyId(String strCompanyId) {
		this.strCompanyId = strCompanyId;
	}
	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}
	/**
	 * @param strUserId the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the strCompanyInfo
	 */
	public String getStrCompanyInfo() {
		return strCompanyInfo;
	}
	/**
	 * @param strCompanyInfo the strCompanyInfo to set
	 */
	public void setStrCompanyInfo(String strCompanyInfo) {
		this.strCompanyInfo = strCompanyInfo;
	}

}
