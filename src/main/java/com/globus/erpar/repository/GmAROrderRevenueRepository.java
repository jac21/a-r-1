package com.globus.erpar.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globus.erpar.model.GmAROrderRevenueModel;

@Repository
public interface GmAROrderRevenueRepository extends JpaRepository<GmAROrderRevenueModel, Integer> {

	
	Optional<GmAROrderRevenueModel>  findByStrOrderIdAndStrVoidFLIsNull(String strOrderId);
   
 
}
