package com.globus.erpar.dao;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.ParameterMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.globus.erpar.rowmapper.GmARCustPOFormatRowMapper;
import  com.globus.erpar.rowmapper.GmARCustPORowMapper;
import com.globus.erpar.rowmapper.GmARRejectPOEmailRowMapper;
import com.globus.erpar.rowmapper.GmPODrillDownRowMapper;
import com.globus.erpar.rowmapper.GmPORowMapper;
import com.globus.erpar.rowmapper.GmARPOStatusRowMapper;
import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.model.GmARRejectPOEmailModel;
import com.globus.erpar.repository.GmAROrderRevenueRepository;
import com.globus.erpar.repository.GmAROrderPORepository;
import com.globus.erpar.model.GmAROrderPOModel;

import org.springframework.jdbc.core.RowMapper;
/**
 * This GmARCustPODaoImpl class used to load/save the Order Revenue Details,Order PO Details and PO Status. 
 * 
 */
@Transactional
@Repository
@Component
public class GmARCustPODaoImpl implements GmARCustPODao{
	@Autowired
	GmAROrderRevenueRepository gmAROrderRevenueRepository;
	
	@Autowired
	GmAROrderPORepository gmAROrderPORepository;

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public GmARCustPODaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	/**
	 * This method is used to load the PO report details
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchPendingPODetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		
		
		String strAmount = gmARCustPOModel.getStrTotal();
		String strLanguageId = gmARCustPOModel.getStrLanguageId();
		String strPONumber = GmCommonClass.parseNull(gmARCustPOModel.getStrPONumber());
		String strDOId = GmCommonClass.parseNull(gmARCustPOModel.getStrOrderId());
		String strPOStatusList	= gmARCustPOModel.getStrPOStatusList();
		
	
		if(!strPONumber.equals("")){
			strPONumber = strPONumber.replaceAll(",", "','");
		}
		if(!strDOId.equals("")){
			strDOId = strDOId.replaceAll(",", "','");
		}
		
		String strPODtlsOuterJoin ="";
		if(strPOStatusList.equals("109540") || strPOStatusList.equals("109541") || strPOStatusList.equals("109548")) // 109540-Draft, 109541-Pending Approval, 109548-Processor Error
		{
			strPODtlsOuterJoin = "";
		}
		else
		{
			strPODtlsOuterJoin = "(+)";
		}
		
		
		StringBuilder strQuery = new StringBuilder();
		 
		strQuery.append("  Select T501.C704_ACCOUNT_ID strAccId,DECODE (" + strLanguageId + ", '103097', NVL (T704.C704_ACCOUNT_NM_EN, T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM)strRepAcc, ");
				strQuery.append("  T501.C501_ORDER_ID strOrderId, t501.C501_DO_DOC_UPLOAD_FL DO_FLAG, T501.C501_ORDER_DATE_TIME ORDDTREPORT, ");
				strQuery.append("  T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME created_by, ");
				strQuery.append("  TO_CHAR (T501.C501_ORDER_DATE, '"+gmARCustPOModel.getStrDtFormat()+"') strOrdDT,GM_PKG_AR_PO_TRANS.GET_ORDER_TOTAL_AMT_BY_PO (T501.C501_ORDER_ID, T501.C501_CUSTOMER_PO) strTotal, ");
				strQuery.append("  DECODE (" + strLanguageId + ", '103097', NVL ( T703.C703_SALES_REP_NAME_EN, T703.C703_SALES_REP_NAME), T703.C703_SALES_REP_NAME) strSalesRep,");
				strQuery.append("  t703.C703_SALES_REP_ID rid,DECODE (" + strLanguageId + ", '103097', NVL ( t701.c701_distributor_name_en, t701.c701_distributor_name),t701.c701_distributor_name)DNAME, ");
				strQuery.append("  T501.C501_PARENT_ORDER_ID strParentOrdId,t701.c701_distributor_id DID, T501.C501_CUSTOMER_PO strPONumber, ");
				strQuery.append("  T701.C701_Distributor_Name strFieldSales, DECODE (T501.C501_PARENT_ORDER_ID, NULL, 'N', 'Y') CHILD_ORDER_FL, ");
				strQuery.append("  t101p.c101_party_nm strParentAcc,t901.c901_code_nm strOrdertype, C501_HOLD_FL strHoldFl, T704.C101_PARTY_ID strParentAccId, t901a.C901_CODE_NM strOrdMode,");
				strQuery.append("  TO_CHAR (T501.C501_SURGERY_DATE, '"+gmARCustPOModel.getStrDtFormat()+"') strSurgeryDt, t101a.C101_USER_F_NAME || ' ' || t101a.C101_USER_L_NAME strADName, t101b.C101_USER_F_NAME || ' ' || t101b.C101_USER_L_NAME strVPName, ");
				strQuery.append("  TO_CHAR (NVL(po_dtls.po_date,T501.C501_CUSTOMER_PO_DATE), '"+gmARCustPOModel.getStrDtFormat()+"') strPOEnteredDt, ");
				strQuery.append("  t101_pouptby.C101_USER_F_NAME || ' ' || t101_pouptby.C101_USER_L_NAME strPOEnteredBy ");
				strQuery.append("  FROM T501_ORDER T501 ,T701_DISTRIBUTOR t701,T703_SALES_REP t703,T704_ACCOUNT T704,T101_USER T101,t901_code_lookup t901, t101_user t101a,  t101_user t101b,t901_code_lookup t901a, t101_party t101p, t101_user t101_pouptby, ");
				strQuery.append("  (select t5001.C704_ACCOUNT_ID accid,t5001.C5001_CUST_PO_ID custpoid,t5001.c501_order_id po_ord_id, t5001.c5001_cust_po_date po_date, t5001.C5001_CUST_PO_UPDATED_BY po_upd_by from T5001_ORDER_PO_DTLS t5001 where t5001.C901_CUST_PO_STATUS IN ("+strPOStatusList+") and c5001_void_fl IS NULL)po_dtls"); //Draft-109540,109541-Pending Approval
				strQuery.append("  WHERE T501.C501_STATUS_FL IS NOT NULL ");
				strQuery.append("  AND ( C501_CUSTOMER_PO  IS NULL  OR (T501.C501_CUSTOMER_PO = po_dtls.custpoid AND T501.C704_ACCOUNT_ID = po_dtls.accid))");
				strQuery.append("  AND T501.C501_CUSTOMER_PO = po_dtls.custpoid"+strPODtlsOuterJoin);
				strQuery.append("  AND T501.C704_ACCOUNT_ID = po_dtls.accid"+strPODtlsOuterJoin);
				strQuery.append("  AND T704.C101_Party_Id = T101p.C101_Party_Id ");
				strQuery.append("  AND po_dtls.po_upd_by = t101_pouptby.c101_user_id (+) ");
				strQuery.append("  AND T704.C704_Void_Fl IS NULL AND T101p.C101_Void_Fl IS NULL");
				strQuery.append("  AND C503_INVOICE_ID IS NULL AND t501.C703_SALES_REP_ID  = t703.C703_SALES_REP_ID ");
				strQuery.append(" AND t701.C701_DISTRIBUTOR_ID  = t703.C701_DISTRIBUTOR_ID AND T704.C704_ACCOUNT_ID = T501.C704_ACCOUNT_ID ");
				strQuery.append(" And T501.C501_Created_By = T101.C101_User_Id AND T501.C501_DELETE_FL IS NULL AND t901a.c901_void_fl(+) IS NULL ");
				strQuery.append(" AND t501.c501_ad_id = t101a.c101_user_id AND t501.c501_vp_id = t101b.c101_user_id AND t901a.c901_code_id(+) = t501.c501_receive_mode ");
				strQuery.append(" AND (t701.c901_ext_country_id IS NULL OR t701.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))");
				strQuery.append(" AND (t703.c901_ext_country_id IS NULL OR t703.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))");
				strQuery.append(" AND (t501.c901_ext_country_id IS NULL OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))");
				strQuery.append(" AND t901.c901_code_id = NVL(t501.c901_order_type,2521)" );
				strQuery.append(" AND T501.C501_VOID_FL IS NULL AND NVL(T501.C901_order_type,-9999) NOT IN ");
				strQuery.append(" (SELECT t906.c906_rule_value FROM t906_rules t906  WHERE t906.c906_rule_grp_id = 'EXC_PO_ORD_TYP' AND c906_rule_id= 'PO_ORDTYPE') ");
											
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrCurrency()).equals("")){
								strQuery.append(" AND T704.C901_CURRENCY = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrCurrency()) +"' ");
							}
							//Adding filter condition
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()).equals("0")){
								strQuery.append(" AND T703.C703_SALES_REP_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()).equals("0")){
								strQuery.append(" AND T704.C101_PARTY_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()).equals("0")){
								strQuery.append(" AND T701.C701_DISTRIBUTOR_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()).equals("0")){
								strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrTotal()).equals("")){
								strQuery.append(" AND GM_PKG_AR_PO_TRANS.GET_ORDER_TOTAL_AMT_BY_PO(T501.C501_ORDER_ID,T501.C501_CUSTOMER_PO) >= "+ strAmount );
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrStartDate()).equals("")){
								strQuery.append(" AND T501.C501_Surgery_Date >= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrStartDate())+"','"+GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat())+"') ");
							} 	
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrEndDate()).equals("")){
								strQuery.append(" AND T501.C501_Surgery_Date <= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrEndDate())+"','"+GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat())+"') ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrOrdStartDate()).equals("")){
								strQuery.append(" AND T501.C501_ORDER_DATE >= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrOrdStartDate())+"','"+GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat())+"') ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrOrdEndDate()).equals("")){
								strQuery.append(" AND T501.C501_ORDER_DATE <= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrOrdEndDate())+"','"+GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat())+"') ");
							}
							
							if(!strDOId.equals("")){
								strQuery.append(" AND UPPER(T501.C501_ORDER_ID) in ('"+ strDOId +"') ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrAccid()).equals("")){
								strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrAccid()) +"' ");
							}
							
							if(!strPONumber.equals("")){
								strQuery.append("  AND UPPER(t501.c501_customer_po) in ('" +strPONumber +"') ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrOrdReceiveMode()).equals("")){
								strQuery.append("  AND T501.C501_RECEIVE_MODE = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrOrdReceiveMode()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrCompanyId()).equals("")){
								strQuery.append(" AND T501.C1900_Company_Id = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrCompanyId()) +"' ");
							}
							
							if(!GmCommonClass.parseNull(gmARCustPOModel.getStrDivisionId()).equals("")){
								strQuery.append(" AND T501.C1910_DIVISION_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrDivisionId()) +"' ");
							}
							
							strQuery.append(" AND NVL (c901_order_type,-9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906 ");
							strQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' AND c906_rule_id= 'EXCLUDE') ");
							strQuery.append(" Order By T704.C704_Account_Nm,T501.C501_ORDER_DATE DESC ");
		
		List<GmARCustPOModel> loadPoList = this.jdbcTemplate.query(strQuery.toString(),new GmPORowMapper());
		
		System.out.println("strQuery.toString()------------------>>> "+strQuery.toString());
		
		
		return loadPoList;
	}
	/**
	 * This method is used to fetch the Order and PO Details for selected Order
	 * @param GmARCustPOModel
	 * @return GmARCustPOModel
	 */
	public List<GmARCustPOModel> fetchOrderDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT t501.c501_customer_po CUSTPO,t501.c501_po_amount POAMT,t501.c501_order_id ORDID,t501.c501_hold_fl HOLDFL,t501.c901_order_type ORDTYP");
		strQuery.append(" ,DECODE(t501.c901_order_type,NULL,'Bill & Ship',get_code_name(t501.c901_order_type)) ORDTYPENAME, t501.c501_receive_mode strOrdReceiveMode");
		strQuery.append(" ,gm_pkg_ar_po_trans.get_order_total_amt_by_po(t501.c501_order_id,t501.c501_customer_po) TOTAL,gm_pkg_ar_po_trans.get_order_ship_amt(t501.c501_order_id,t501.c501_customer_po) SHIPCOST,Get_Back_Order_Cnt(t501.c501_order_id) BCKORDCNT");
		strQuery.append(" ,DECODE(t501.c901_order_type,'26240232',gm_pkg_ar_po_trans.get_child_orders_id(t501.c501_order_id,''),'') DIRBCKORDIDS");
		strQuery.append(" ,NVL(t5001.c5001_order_po_dtls_id,chidord.podtlid) PODTLID,t501.c704_account_id strAccId,nvl(t5001.C901_CUST_PO_STATUS,chidord.postatus) POSTATUS");
		strQuery.append(" FROM t501_order t501,t5001_order_po_dtls t5001");
		strQuery.append(" ,(SELECT t5001.C901_CUST_PO_STATUS postatus,t5001.C5001_ORDER_PO_DTLS_ID podtlid,t501.c501_order_id orderid from t5001_order_po_dtls t5001,t501_order t501");
		strQuery.append(" WHERE t501.c501_parent_order_id IS NOT NULL");
		strQuery.append(" AND t501.c501_parent_order_id = t5001.c501_order_id");
		strQuery.append(" AND t501.c501_void_fl IS NULL");
		strQuery.append(" AND t5001.c5001_void_fl IS NULL");
		strQuery.append(" AND  t501.c501_order_id='"+GmCommonClass.parseNull(gmARCustPOModel.getStrOrderId())+"'");
		strQuery.append(" AND t501.c501_customer_po IS NOT NULL) chidord");
		strQuery.append(" WHERE t501.c501_order_id = t5001.c501_order_id(+)");
		strQuery.append(" AND t501.c501_order_id =chidord.orderid(+)");
		strQuery.append(" AND t501.c501_void_fl IS NULL");
		strQuery.append(" AND t5001.c5001_void_fl(+) IS NULL");
		strQuery.append(" AND t501.c501_order_id='"+GmCommonClass.parseNull(gmARCustPOModel.getStrOrderId())+"'");
		List<GmARCustPOModel> loadPoList = this.jdbcTemplate.query(strQuery.toString(),new GmARCustPORowMapper());
		return loadPoList;
	}
	/**
	 * This method is used to fetch the Order Revenue Details for selected Order
	 * @param GmAROrderRevenueModel
	 * @return GmAROrderRevenueModel
	 */
	public GmAROrderRevenueModel   fetchOrderRevenueDetails(GmAROrderRevenueModel gmAROrderRevenueModel) throws Exception{
		Optional<GmAROrderRevenueModel> loadPoDetails =  gmAROrderRevenueRepository.findByStrOrderIdAndStrVoidFLIsNull(gmAROrderRevenueModel.getStrOrderId());
		if(loadPoDetails.isPresent()){
			gmAROrderRevenueModel = loadPoDetails.get();
		}
		return gmAROrderRevenueModel;
	}
	/**
	 * This method used to save/update Order Revenue Details for selected Order
	 * @param - GmARCustPOModel
	 * @return - GmAROrderRevenueModel
	 */	
	@Override
	public Optional<GmAROrderRevenueModel> saveOrderRevenueDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		String strPOQuery = saveOrderPORevenueDetails(gmARCustPOModel);
		Optional<GmAROrderRevenueModel> loadPoDetails =  gmAROrderRevenueRepository.findByStrOrderIdAndStrVoidFLIsNull(gmARCustPOModel.getStrOrderId());
		return loadPoDetails;
		
	}
	/**
	 * This method is used to save/update Order PO Details
	 * @param - GmARCustPOModel
	 * @return - GmARCustPOModel
	 */		
	@Override
	public List<GmARCustPOModel> saveOrderPODetails(GmARCustPOModel gmARCustPOModel) throws Exception{
		String strPOQuery = saveOrderPORevenueDetails(gmARCustPOModel);
		List<GmARCustPOModel> loadPoList = fetchOrderDetails(gmARCustPOModel);
        return loadPoList;
	}
	/**
	 * This method used to validate the current PO with Existing PO
	 * @param - strPONumber,strCompanyId
	 * @return - Count as String
	 */
	public String validatePO(String strPONumber,String strCompanyId,String strOrderId)  throws Exception{
		 
		 String strPOQuery =" SELECT COUNT (1)    FROM t501_order"
				 +" WHERE c501_customer_po   = '"+strPONumber
				 +"' AND c501_delete_fl IS NULL "
				 +" AND c501_void_fl IS NULL "
				 +" AND( c501_order_id !='"+strOrderId+"' OR c501_parent_order_id  !='"+strOrderId+"' )"
				 +" AND (c901_ext_country_id IS NULL OR c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) "
				 +" AND c1900_company_id =  "+strCompanyId;
		 
		
		 Query poQuery = em.createNativeQuery(strPOQuery);
			return  ((Number) poQuery.getSingleResult()).toString();
	 }
	 
	 /**
	  * This method is used to fetch the Existing PO Details for entered PO Number
	  * @param GmARCustPOModel
	  * @return GmARCustPOModel
	  */
	public List<GmARCustPOModel> fetchPODrilldown(GmARCustPOModel gmARCustPOModel) throws Exception{
		String strPODrill =  " SELECT C501_ORDER_ID strOrderId,  TO_CHAR(C501_ORDER_DATE_TIME,'"+gmARCustPOModel.getStrDtFormat()+"') strOrdDT, "
				+" gm_pkg_cm_shipping_info.get_track_no(c501_order_id,50180) strShipTrack, gm_pkg_ar_po_trans.get_order_total_amt_by_po(C501_ORDER_ID,C501_CUSTOMER_PO) strTotal,"
				+" T704.C704_ACCOUNT_NM strRepAcc,GET_USER_NAME(C501_CREATED_BY) strUserName,SUBSTR(C501_CUSTOMER_PO,0,20) strPONumber,T503.C503_INVOICE_ID strInvoice,"
				+" get_code_name(C901_ORDER_TYPE) strOrdertype,C501_PARENT_ORDER_ID strParentOrdId,TO_CHAR(C501_SHIPPING_DATE,'"+gmARCustPOModel.getStrDtFormat()+"') strShipDt "
				+" FROM T501_ORDER T501,T704_ACCOUNT T704,T503_INVOICE T503 "
				+" WHERE C501_VOID_FL IS NULL AND T501.C503_INVOICE_ID = T503.C503_INVOICE_ID(+) "
				+" AND T503.C503_VOID_FL (+) IS NULL AND T501.C704_ACCOUNT_id = T704.C704_ACCOUNT_id "
				+" AND (C501_DELETE_FL  IS NULL  OR (C901_ORDER_TYPE  = '102080' AND C501_DELETE_FL  ='Y' )) "
				+" AND ( T501.C901_ext_country_id IS NULL OR T501.C901_ext_country_id    IN (SELECT country_id FROM V901_COUNTRY_CODES))"
				+" AND C501_CUSTOMER_PO='"+gmARCustPOModel.getStrCustPONumber()+"' AND NVL (c901_order_type, - 9999) NOT IN (SELECT t906.c906_rule_value "
				+" FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDTYPE' AND c906_rule_id = 'EXCLUDE' ) "
				+" AND T501.C1900_COMPANY_ID ="+gmARCustPOModel.getStrCompanyId()+" ORDER BY C501_ORDER_DATE_TIME DESC ";

List<GmARCustPOModel> loadPoDrill = this.jdbcTemplate.query(strPODrill,new GmPODrillDownRowMapper());

return loadPoDrill;
	}
	/**
	 * This method is used to save/update Order PO Details and Revenue Details
	 * @param GmARCustPOModel
	 * @return String
	 */	
		public String saveOrderPORevenueDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
            String strOrdId = GmCommonClass.parseNull(gmARCustPOModel.getStrOrderId());
            String strPOAmt = GmCommonClass.parseNull(gmARCustPOModel.getStrPOAmt());
            String strPONum = GmCommonClass.parseNull(gmARCustPOModel.getStrCustPONumber());
            String strPODtls = GmCommonClass.parseNull(gmARCustPOModel.getStrPODtls());
            String strDODtls = GmCommonClass.parseNull(gmARCustPOModel.getStrDODtls());
            String strUserId = GmCommonClass.parseNull(gmARCustPOModel.getStrUserId());
			try{
				StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_update_po")
						.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(4,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(5,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(6,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(7,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(8,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(9,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(10,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(11,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(12,String.class,ParameterMode.OUT)
			    		.setParameter(1, strOrdId)
				        .setParameter(2, strPONum)
				        .setParameter(3, strUserId)
				        .setParameter(4, "")
				        .setParameter(5, "")
				        .setParameter(6, strPOAmt)
				        .setParameter(7, "")
				        .setParameter(8, "")
				        .setParameter(9, "")
				        .setParameter(10, strPODtls)
				        .setParameter(11, strDODtls);
				        //
				        storedProcedureQuery.execute();
				        String strComments = (String) storedProcedureQuery.getOutputParameterValue(12);
				      

	        return strComments;
			}catch(Exception e){
				e.printStackTrace();
				return "";
			}
			
			
		}
		/**
		 * This method is used to save/update PO Status(Ready For Invoicing,Move to Discrepancy,Reject)
		 * @param GmARCustPOModel
		 * @return GmARCustPOModel
		 */
		public void saveOrderPOStatus(GmARCustPOModel gmARCustPOModel) throws Exception{
			
			try{
			StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_po_trans.gm_upd_po_status")
					.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(4,String.class,ParameterMode.IN)
		    		.setParameter(1, gmCommonClass.parseNull(gmARCustPOModel.getStrAccid()))
		    		.setParameter(2, gmCommonClass.parseNull(gmARCustPOModel.getStrCustPONumber()))
				    .setParameter(3, gmCommonClass.parseNull(gmARCustPOModel.getStrPOStatus()))
				    .setParameter(4, gmCommonClass.parseNull(gmARCustPOModel.getStrUserId()));
			
			 	storedProcedureQuery.execute();
			}catch(Exception e){
				e.printStackTrace();
			}	
			 	
		}
		/**
		 * Description: This method is used to get report PO status Report
		 * Author
		 * @param GmARCustPOModel
		 * @return List<GmARCustPOModel>
		 * @exception Exception 
		 **/
		public List<GmARCustPOModel> fetchReadyForInvoiceDtls(GmARCustPOModel gmARCustPOModel) throws Exception{
			StringBuilder strQuery = new StringBuilder();
			String strDateFormat = "";
			strDateFormat = GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat());	
			strQuery.append(" SELECT READYFORINVDTLS.*, NVL(strOrderAmt,0) - NVL(strPOAmt,0) strDiffAmt FROM (");
			strQuery.append(" SELECT T5001.C5001_ORDER_PO_DTLS_ID strPODetaiId,get_parent_account_name(T501.C704_ACCOUNT_ID) strParentAcc, T501.C704_ACCOUNT_ID strParentAccId, ");
			strQuery.append(" T501.C501_ORDER_ID strOrderId,T501.C501_CUSTOMER_PO strCustPO,gm_pkg_ar_po_trans.get_order_total_amt_by_po(T501.C501_ORDER_ID,");
			strQuery.append(" T501.C501_CUSTOMER_PO) strOrderAmt, T501.C501_PO_AMOUNT strPOAmt,T101.C101_PARTY_NM strPartyName,T704.C704_ACCOUNT_NM strAccountNm,");
			strQuery.append(" T701.C701_DISTRIBUTOR_NAME strDistNm,T703.C703_SALES_REP_NAME strSalesRepNm,T501.C501_HOLD_FL strHoldFl,T501.C501_HOLD_FL strChkHoldfl,");
			strQuery.append(" T501.C501_DO_DOC_UPLOAD_FL strUploadFl,T1910.C1910_DIVISION_NAME strDivisionNm,T5001.C901_CUST_PO_STATUS strPOStatus , ");
			strQuery.append(" GET_USER_NAME(T5001.C5001_PO_STATUS_UPDATED_BY) strPOStatusUpdatedBy, TO_CHAR (T5001.C5001_PO_STATUS_UPDATED_DATE, '"+gmARCustPOModel.getStrDtFormat()+"') strPOStatusUpdatedDate,  T501.C501_RECEIVE_MODE strOrdReceiveMode, ");
			strQuery.append(" TO_CHAR (NVL(T5001.C5001_CUST_PO_DATE,T501.C501_CUSTOMER_PO_DATE), '"+strDateFormat+"') strPOEnteredDt, ");
			strQuery.append(" t101_pouptby.C101_USER_F_NAME || ' ' || t101_pouptby.C101_USER_L_NAME strPOEnteredBy ");
			strQuery.append(" FROM T5001_ORDER_PO_DTLS  T5001,T501_ORDER T501,T704_ACCOUNT T704,T701_DISTRIBUTOR T701,T703_SALES_REP T703,T1910_DIVISION T1910,T101_PARTY T101, t101_user t101_pouptby");
			strQuery.append(" WHERE T5001.C501_ORDER_ID = T501.C501_ORDER_ID ");
			strQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID  ");
			strQuery.append(" AND T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID ");
			strQuery.append(" AND T501.C703_SALES_REP_ID = T703.C703_SALES_REP_ID ");
			strQuery.append(" AND T501.C1910_DIVISION_ID = T1910.C1910_DIVISION_ID ");
			strQuery.append(" AND T704.C101_PARTY_ID = T101.C101_PARTY_ID ");
			strQuery.append(" AND t5001.c5001_cust_po_updated_by = t101_pouptby.c101_user_id ");
			strQuery.append(" AND T501.C501_VOID_FL IS NULL ");
			strQuery.append(" AND T5001.C5001_VOID_FL IS NULL ");
			strQuery.append(" AND T704.C704_VOID_FL IS NULL ");
			strQuery.append(" AND T703.C703_VOID_FL IS NULL ");
			strQuery.append(" AND T1910.C1910_VOID_FL IS NULL ");
			strQuery.append(" AND T101.C101_VOID_FL IS NULL ");
			strQuery.append(" AND T501.C503_INVOICE_ID IS NULL ");
			strQuery.append(" AND T5001.C901_CUST_PO_STATUS IN ( "+gmARCustPOModel.getStrPOStatusList()+ ") ");
			
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()).equals("0")){
						strQuery.append(" AND T701.C701_DISTRIBUTOR_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrFieldSales()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()).equals("0")){
						strQuery.append(" AND T703.C703_SALES_REP_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrSalesRep()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrDivisionId()).equals("")){
						strQuery.append(" AND T501.C1910_DIVISION_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrDivisionId()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrCompanyId()).equals("")){
						strQuery.append(" AND T501.C1900_COMPANY_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrCompanyId()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrCustPONumber()).equals("")){
						strQuery.append(" AND UPPER(T501.C501_CUSTOMER_PO) = UPPER('"+ GmCommonClass.parseNull(gmARCustPOModel.getStrCustPONumber()) +"') ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()).equals("0")){
						strQuery.append(" AND T704.C101_PARTY_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrParentAcc()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()).equals("") & !GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()).equals("0")){
						strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrRepAcc()) +"' ");
					}
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrAccid()).equals("")){
						strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARCustPOModel.getStrAccid()) +"' ");
					}
					
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrPOEnteredFromDate()).equals("")){
						strQuery.append(" AND trunc(T501.C501_CUSTOMER_PO_DATE) >= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrPOEnteredFromDate())+"','"+strDateFormat+"') ");
					}
					
					if(!GmCommonClass.parseNull(gmARCustPOModel.getStrPOEnteredToDate()).equals("")){
						strQuery.append(" AND trunc(T501.C501_CUSTOMER_PO_DATE) <= to_date('"+GmCommonClass.parseNull(gmARCustPOModel.getStrPOEnteredToDate())+"','"+strDateFormat+"') ");
					}
					
					strQuery.append(" Order By T704.C704_Account_Nm,T501.C501_ORDER_DATE DESC ");
					strQuery.append(")READYFORINVDTLS");
					List<GmARCustPOModel> loadPoList = this.jdbcTemplate.query(strQuery.toString(),new GmARPOStatusRowMapper());
					System.out.println("Ready For Invoicing------------------>>> "+strQuery.toString());
					return loadPoList;
		}

        /**
		 * Description: This method used to save/update PO's discrepancy Details for selected PO
		 * 
		 * @param GmARCustPOModel
		 * @exception Exception 
		 **/
		@Override
		public void saveDiscrepancyDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
			
			StringTokenizer strOrderPODtlListTok = new StringTokenizer(gmARCustPOModel.getStrOrderPODtlIdList(), ",");
			while (strOrderPODtlListTok.hasMoreTokens()) {
				
				String strPOListID = strOrderPODtlListTok.nextToken();
				Optional<GmAROrderPOModel> loadOrderPoDetails =  gmAROrderPORepository.findByStrOrderPODtlIdAndStrVoidFlIsNull(Integer.parseInt(GmCommonClass.parseZero(strPOListID)));
				
				if(loadOrderPoDetails.isPresent()){
					GmAROrderPOModel gmAROrderPOModel = loadOrderPoDetails.get();
					gmARCustPOModel.setStrAccid(gmAROrderPOModel.getStrAccountId());
					gmARCustPOModel.setStrCustPONumber(gmAROrderPOModel.getStrCustPoId());
					gmARCustPOModel.setStrPOStatus("109544"); // Move to Discrepancy
					saveOrderPOStatus(gmARCustPOModel);	
			}
			}		
			
		}
		
		
		/**
		   * Description: This method used to fetch PO Format drill down details based on account
		   * 
		   * @param GmARCustPOModel
		   * @exception Exception 
		   **/
		@Override
		public List<GmARCustPOModel> fetchPOFormatDrilldownDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
			StringBuilder strQuery = new StringBuilder();
			
			strQuery.append(" SELECT t101.c101_party_nm strPartyName,t704.c704_account_id strAccId, t503.c503_invoice_id strInvoiceId, ");
			strQuery.append(" TO_CHAR(T503.c503_invoice_date, '"+gmARCustPOModel.getStrDtFormat()+"') strInvoiceDt,t503.c503_customer_po strCustomerPO, t503.c503_inv_amt strInvoiceAmt");
			strQuery.append(" FROM t503_invoice t503,t704_account t704,t101_party t101 ");
			strQuery.append(" WHERE t503.c704_account_id = t704.c704_account_id ");
			strQuery.append(" AND t704.c101_party_id = t101.c101_party_id ");
			strQuery.append(" AND nvl(t503.c901_invoice_type, - 9999) NOT IN ( ");
			strQuery.append(" SELECT t906.c906_rule_value ");
			strQuery.append(" FROM t906_rules t906 ");
			strQuery.append(" WHERE t906.c906_rule_grp_id = 'INVOICETYPE' ");
			strQuery.append(" AND c906_rule_id = 'EXCLUDE' ");
			strQuery.append(" AND c906_void_fl IS NULL ) ");
			strQuery.append(" AND t503.c503_void_fl IS NULL ");
			strQuery.append(" AND t704.c704_void_fl IS NULL ");
			strQuery.append(" AND t704.c704_account_id='"+ GmCommonClass.parseNull(gmARCustPOModel.getStrAccid()) +"' ");
			strQuery.append(" ORDER BY nvl(t503.c503_last_updated_date,t503.c503_created_date) desc Offset 0 ROWS");
			strQuery.append(" FETCH NEXT 10 ROWS ONLY ");
			List<GmARCustPOModel> loadPOFormatDtls = this.jdbcTemplate.query(strQuery.toString(),new GmARCustPOFormatRowMapper());
			System.out.println("PO Format Fetch Query=>"+strQuery.toString());
			return loadPOFormatDtls;
		}
/**
		   * Description: This method used to fetch Reject PO Notify Email details
		   * 
		   * @param GmARCustPOModel
		   * @exception Exception 
		   **/
		@Override
		public List<GmARRejectPOEmailModel> fetchRejectPONotifyEmailDtls(GmARRejectPOEmailModel gmARRejectPOEmailModel) throws Exception{
			StringBuilder strQuery = new StringBuilder();
					  
			String strOrderId = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrOrderId());
			String strCompId = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrCompanyId());
			String strCanIdReason = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrCanceIdNm());
			String strCustPONum = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrCustPONumber());
			String strRejCmnt = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrComment());
			String strRejBy = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrPORejectBy());
			
			
			  String strDtFormat = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrDtFormat());
		      String strCompZone = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrCompZone());
			 //to get current date based on company date format and time zone
	        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(strCompZone));
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(strDtFormat);
	        String strLocalDt = GmCommonClass.parseNull(formatter.format(zonedDateTime.toLocalDate()));
	        
			strQuery.append(" select t107.c107_contact_value poRejectedTo, t704.c704_account_id poRejectedAccId, t704.c704_account_nm poRejectedAccNm, ");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_CC', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectedCc, ");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_FROM', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectedFrom, ");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_SUBJECT', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectedSubject,");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_CONTYP', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectedConType, ");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_TITLE', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectedTitle, ");
			strQuery.append(" get_rule_value_by_company('AR_REJECT_PO_BODY', 'AR_REJECT_PO_EMAIL',"+strCompId+") poRejectBdy,");
			strQuery.append(" get_rule_value('AR_REJECT_PO_HEADER_FL', 'AR_REJECT_PO_EMAIL') poRejectHeaderFl ");
			strQuery.append(" from t501_order t501,t703_sales_rep t703,t101_party t101p,t107_contact t107,t704_account t704 ");
			strQuery.append(" where t501.c704_account_id = t704.c704_account_id ");
			strQuery.append(" and t501.c703_sales_rep_id = t703.c703_sales_rep_id ");
			strQuery.append(" and t703.c101_party_id = t101p.c101_party_id ");
			strQuery.append(" and t101p.c101_party_id = t107.c101_party_id(+) ");
			strQuery.append(" and t107.c901_mode(+) = 90452 "); // 90452 - Email
			strQuery.append(" and t501.c501_order_id = '"+strOrderId+"' ");
			strQuery.append(" and t107.c107_primary_fl(+) = 'Y' ");
			strQuery.append(" and t501.c501_void_fl is null ");
			strQuery.append(" and t703.c703_void_fl is null ");
			strQuery.append(" and t101p.c101_void_fl is null ");
			strQuery.append(" and t107.c107_void_fl is null ");
			strQuery.append(" and t704.c704_void_fl is null ");
						
			List<GmARRejectPOEmailModel> loadRejectPOEmailDtls = this.jdbcTemplate.query(strQuery.toString(),new GmARRejectPOEmailRowMapper());
			System.out.println("PO Reject Email Fetch Query=>"+strQuery.toString());
			for (GmARRejectPOEmailModel valueRejPOEmailDtls : loadRejectPOEmailDtls) {
				gmARRejectPOEmailModel = valueRejPOEmailDtls;
				gmARRejectPOEmailModel.setStrOrderId(strOrderId);
				gmARRejectPOEmailModel.setStrCustPONumber(strCustPONum);
				gmARRejectPOEmailModel.setStrComment(strRejCmnt);
				gmARRejectPOEmailModel.setStrPORejectBy(strRejBy);
				gmARRejectPOEmailModel.setStrPOLocalDt(strLocalDt);
				gmARRejectPOEmailModel.setStrCanceIdNm(strCanIdReason);
				//to concate account id and account name
				String strAccidAndNm = GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrPORejectAccID()) +" - "+ GmCommonClass.parseNull(gmARRejectPOEmailModel.getStrPORejectAccNm());
				gmARRejectPOEmailModel.setStrPOAccidAndNm(strAccidAndNm);
				//this function call used to replace email body content
				replaceEmailBdyContent(gmARRejectPOEmailModel);
			}
			
			return loadRejectPOEmailDtls;
			
			
		}
		
		public void replaceEmailBdyContent(GmARRejectPOEmailModel gmEmailARRejectPOEmailModel)throws Exception
		{
			
			String strRejEmailBdyCont = "";
			String strRejEmailHeader ="";
			
			strRejEmailBdyCont = GmCommonClass.parseNull(gmEmailARRejectPOEmailModel.getStrPORejectBody());
			
			if(!strRejEmailBdyCont.equals(""))
				{
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<REC_BY>", GmCommonClass.parseNull(gmEmailARRejectPOEmailModel.getStrPORejectBy()));
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<PO_REASON>", GmCommonClass.parseNull(gmEmailARRejectPOEmailModel.getStrCanceIdNm()));
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<REJ_DT>", gmEmailARRejectPOEmailModel.getStrPOLocalDt());
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<ACC_ID>", gmEmailARRejectPOEmailModel.getStrPOAccidAndNm());
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<PO_ID>", gmEmailARRejectPOEmailModel.getStrCustPONumber());
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<DO_ID>", gmEmailARRejectPOEmailModel.getStrOrderId());
				strRejEmailBdyCont = strRejEmailBdyCont.replaceAll("#<PO_COM>", gmEmailARRejectPOEmailModel.getStrComment());

				}
				
			gmEmailARRejectPOEmailModel.setStrPORejectBody(strRejEmailBdyCont);
				
		}
		
		/**
		 * Description: This method used to move PO's into Processor Error for selected PO
		 * 
		 * @param GmARCustPOModel
		 * @exception Exception 
		 **/
		@Override
		public void saveProcessorErrorDetails(GmARCustPOModel gmARCustPOModel) throws Exception{
			StringTokenizer strOrderPODtlListTok = new StringTokenizer(gmARCustPOModel.getStrOrderPODtlIdList(), ",");
			while (strOrderPODtlListTok.hasMoreTokens()) {
				
				String strPOListID = strOrderPODtlListTok.nextToken();
				Optional<GmAROrderPOModel> loadOrderPoDetails =  gmAROrderPORepository.findByStrOrderPODtlIdAndStrVoidFlIsNull(Integer.parseInt(GmCommonClass.parseZero(strPOListID)));
				
				if(loadOrderPoDetails.isPresent()){
					GmAROrderPOModel gmAROrderPOModel = loadOrderPoDetails.get();
					gmARCustPOModel.setStrAccid(gmAROrderPOModel.getStrAccountId());
					gmARCustPOModel.setStrCustPONumber(gmAROrderPOModel.getStrCustPoId());
					gmARCustPOModel.setStrPOStatus("109548"); //Processor Error
					saveOrderPOStatus(gmARCustPOModel);	
			}
			}	
			
		}
}
