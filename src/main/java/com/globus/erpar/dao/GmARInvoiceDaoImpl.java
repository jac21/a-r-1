package com.globus.erpar.dao;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.ParameterMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.globus.erpar.rowmapper.GmAROrderbyPORowMapper;
import  com.globus.erpar.rowmapper.GmARCustPORowMapper;
import com.globus.erpar.rowmapper.GmAROrderbyPODetailRowMapper;
import com.globus.erpar.rowmapper.GmPODrillDownRowMapper;
import com.globus.erpar.rowmapper.GmPORowMapper;
import com.globus.common.dao.GmEntityManager;
import com.globus.common.model.GmUser;
import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARInvoiceModel;
import com.globus.erpar.model.GmAROrderRevenueModel;
import com.globus.erpar.repository.GmAROrderRevenueRepository;

import org.springframework.jdbc.core.RowMapper;

@Transactional
@Repository
@Component
public class GmARInvoiceDaoImpl extends GmEntityManager implements GmARInvoiceDao{
	@Autowired
	GmAROrderRevenueRepository gmAROrderRevenueRepository;

	@Autowired
	GmEntityManager gmEntityManager;
	
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public GmARInvoiceDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	/**
	 * This method is used to create the batch.
	 * @param String
	 * @return String
	 */	
		public String saveBatchProcess(GmARInvoiceModel gmARInvoiceModel) throws Exception{
			String strCompanyId = GmCommonClass.parseNull(gmARInvoiceModel.getStrCompanyId());
			String strUserId = GmCommonClass.parseNull(gmARInvoiceModel.getStrUserId());
			String inputStr = GmCommonClass.parseNull(gmARInvoiceModel.getInputStr());
			String strPlantId = GmCommonClass.parseNull(gmARInvoiceModel.getStrPlantId());
			String strCompZone = GmCommonClass.parseNull(gmARInvoiceModel.getStrCompZone());
		
			gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
			
			
			try{
				StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_process_batch.gm_add_process_batch_po_dtls")
						.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
			    		.registerStoredProcedureParameter(4,String.class,ParameterMode.OUT)
			    		.setParameter(1, inputStr)
				        .setParameter(2, strCompanyId)
				        .setParameter(3, strUserId);
				
				        storedProcedureQuery.execute();
				        String strBatchId = (String) GmCommonClass.parseNull(storedProcedureQuery.getOutputParameterValue(4));
				      
				        System.out.println("strBatchId====="+strBatchId);
	        return strBatchId;
			}catch(Exception e){
				e.printStackTrace();
				return "";
			}
			
			
		}
		/**
		 * This method is used to fetching the orders by PODtlsId for Associated Orders popup
		 */	
	@Override
	public List<GmARCustPOModel> fetchOrdersByPO(GmARCustPOModel gmARCustPOModel) {
		String strPODtlId=GmCommonClass.parseNull(gmARCustPOModel.getStrPONumDtlsId());
		StringBuilder strPOQuery = new StringBuilder();
		strPOQuery.append(" Select t5001.c5001_cust_po_id CUSTPO, t5001.c5001_cust_po_amt CUSTPOAMT, TO_CHAR (t501.c501_customer_po_date, '"+GmCommonClass.parseNull(gmARCustPOModel.getStrDtFormat())+ " HH:MI AM' ) CUSTPODATE,  ");
		strPOQuery.append(" t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME CUSTPOUPDBY");
		strPOQuery.append(" FROM t501_order    t501, t5001_order_po_dtls   t5001, t101_user t101  ");
		strPOQuery.append(" WHERE t5001.c5001_order_po_dtls_id ='"+strPODtlId+"' ");
		strPOQuery.append(" and t5001.c5001_cust_po_id = t501.c501_customer_po ");
		strPOQuery.append(" and t5001.c704_account_id  = t501.c704_account_id ");
		strPOQuery.append(" and t101.c101_user_id = t5001.c5001_cust_po_updated_by ");
		strPOQuery.append(" and t5001.c5001_void_fl is null ");
		strPOQuery.append(" and t501.c501_void_fl is null ");
		List<GmARCustPOModel> orderList = this.jdbcTemplate.query(strPOQuery.toString(),new GmAROrderbyPORowMapper());
		return orderList;
	}
	
	
	/**
	 * This method is used to fetch PODetails by PODtsId for Associated Orders popup
	 */
	@Override
	public List<GmARCustPOModel> fetchPODetails(GmARCustPOModel gmARCustPOModel) {
		String strPODtlId=GmCommonClass.parseNull(gmARCustPOModel.getStrPONumDtlsId());
		StringBuilder strPOQuery = new StringBuilder();
		strPOQuery.append(" Select t501.c501_order_id ORDID, t901.c901_code_nm ORDTYP, t501.c501_total_cost TOTCOST ");
		strPOQuery.append(" FROM t501_order   t501, t5001_order_po_dtls   t5001, t901_code_lookup t901 ");
		strPOQuery.append(" WHERE t5001.c5001_order_po_dtls_id ='"+strPODtlId+"'");
		strPOQuery.append(" and t901.c901_code_id(+) = t501.c901_order_type ");
		strPOQuery.append(" and t5001.c5001_cust_po_id = t501.c501_customer_po ");
		strPOQuery.append(" and t5001.c704_account_id  = t501.c704_account_id ");
		strPOQuery.append(" and t901.c901_void_fl(+) is null ");
		strPOQuery.append(" and t5001.c5001_void_fl is null ");
		strPOQuery.append(" and t501.c501_void_fl is null ");
		List<GmARCustPOModel> ordersList =this.jdbcTemplate.query(strPOQuery.toString(),new GmAROrderbyPODetailRowMapper());

		return ordersList;
	}
		
}
