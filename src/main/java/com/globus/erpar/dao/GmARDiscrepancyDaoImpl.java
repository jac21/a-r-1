package com.globus.erpar.dao;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.globus.common.util.GmCommonClass;
import com.globus.erpar.model.GmARCustPOModel;
import com.globus.erpar.model.GmARDiscrepancyDtlReportModel;
import com.globus.erpar.model.GmARDiscrepancyModel;
import com.globus.erpar.model.GmAROrderPOModel;
import com.globus.erpar.repository.GmAROrderPORepository;
import com.globus.erpar.rowmapper.GmARDiscrepancyRowMapper;
import com.globus.erpar.rowmapper.GmARPODiscrepancyDtlReportRowMapper;
import com.globus.erpar.rowmapper.GmARPOLogRowMapper;
import com.globus.erpar.rowmapper.GmPODrillDownRowMapper;
import com.globus.common.dao.GmEntityManager;
import com.globus.erpar.model.GmARPODiscrepancyLineItemModel;
import com.globus.erpar.rowmapper.GmARDiscrepancyRowMapper;
import com.globus.erpar.rowmapper.GmARPODiscrepancyLineItemRowMapper;


@Transactional
@Repository
@Component
public class GmARDiscrepancyDaoImpl extends GmEntityManager implements GmARDiscrepancyDao {

	
	@Autowired
	GmCommonClass gmCommonClass;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	GmAROrderPORepository gmAROrderPORepository;
	
	@Autowired
	GmEntityManager gmEntityManager;
	
	public GmARDiscrepancyDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	/**
	 * Description: This method is used to get report of discrepancy details Report based on filters
	 * Author
	 * @param GmARDiscrepancyModel
	 * @return List<GmARDiscrepancyModel>
	 * @exception Exception 
	 **/
	public List<GmARDiscrepancyModel> loadDiscrepancyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		String strPONumber = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCustPONumber());        
        if(!strPONumber.equals("")){
            strPONumber = strPONumber.replaceAll(",", "','");
        }
        strPONumber =strPONumber.toUpperCase();
        
        
        String strOrderId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrOrderId());      
        if(!strOrderId.equals("")){
        	strOrderId = strOrderId.replaceAll(",", "','");
        }
        strOrderId =strOrderId.toUpperCase();
        String strDtFormat = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat());
        String strCompZone = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompZone());
       //to get current date based on company date format and time zone
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(strCompZone));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(strDtFormat);
        
        
		StringBuilder strQuery = new StringBuilder();
		strQuery.append(" SELECT DISCREPANCYDTLS.*, NVL(strOrderAmt,0) - NVL(strPOAmt,0) strDiffAmt FROM (");
		strQuery.append(" SELECT T5001.C5001_ORDER_PO_DTLS_ID strPODetaiId, T501.C501_ORDER_ID strOrderId, T501.C501_CUSTOMER_PO strCustPO,");
		strQuery.append(" gm_pkg_ar_po_trans.get_order_total_amt_by_po(T501.C501_ORDER_ID,T501.C501_CUSTOMER_PO) strOrderAmt, ");
		strQuery.append(" T501.C501_PO_AMOUNT strPOAmt, T704.C704_ACCOUNT_NM strRepAccNm, T703.C703_SALES_REP_NAME strSalesRepNm, nvl(t901a.c901_code_nm , t901b.c901_code_nm) strCollectorNm, ");
		strQuery.append(" t901.C901_CODE_NM strCategoryNM,t101.c101_party_nm strParentAcc,TO_CHAR (T501.C501_ORDER_DATE, '"+gmARDiscrepancyModel.getStrDtFormat()+"') strOrdDT, ");
		strQuery.append(" T501.C704_ACCOUNT_ID strAccId,TO_CHAR (T5001.C5001_RESOLVED_DISC_DATE,'"+gmARDiscrepancyModel.getStrDtFormat()+"') strResDate,");
		strQuery.append(" TO_CHAR (T5001.C501_PO_MOVE_DISCREPANCY_DATE,'"+gmARDiscrepancyModel.getStrDtFormat()+"') strDisDate,T501.C501_PARENT_ORDER_ID strParentOrdId, ");
		strQuery.append(" t101a.C101_USER_F_NAME || ' ' || t101a.C101_USER_L_NAME strADName, t101b.C101_USER_F_NAME || ' ' || t101b.C101_USER_L_NAME strVPName, ");
		strQuery.append(" NVL(T5001.C901_COLLECTOR_ID,T704A.C704A_ATTRIBUTE_VALUE) collectorid, ");
		strQuery.append(" t501.C501_RECEIVE_MODE ordMode, t5001.C5001_PO_DISC_RESOLVED_FL strPOResolvefl, TRUNC(t5001.C5001_PO_DISC_REMAIND_DATE) - TRUNC(to_date('"+formatter.format(zonedDateTime.toLocalDate())+"','"+strDtFormat+"')) strPODiscRemdate, ");
		strQuery.append(" TO_CHAR (NVL(T5001.C5001_CUST_PO_DATE,T501.C501_CUSTOMER_PO_DATE), '"+gmARDiscrepancyModel.getStrDtFormat()+"') strPOEnteredDt, ");
		strQuery.append(" t101_pouptby.C101_USER_F_NAME || ' ' || t101_pouptby.C101_USER_L_NAME strPOEnteredBy ");
		strQuery.append(" FROM T5001_ORDER_PO_DTLS  T5001,T501_ORDER T501,T704_ACCOUNT T704,T701_DISTRIBUTOR T701,T703_SALES_REP T703,T1910_DIVISION T1910,t101_user t101a,t101_user t101b,T101_PARTY T101, ");
		strQuery.append(" t901_code_lookup  t901,t901_code_lookup  t901a, t901_code_lookup t901b,T704A_ACCOUNT_ATTRIBUTE T704A, t101_user t101_pouptby ");
		strQuery.append(" WHERE T5001.C501_ORDER_ID = T501.C501_ORDER_ID ");
		strQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID  ");
		strQuery.append(" AND T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID ");
		strQuery.append(" AND T501.C703_SALES_REP_ID = T703.C703_SALES_REP_ID ");
		strQuery.append(" AND T501.C1910_DIVISION_ID = T1910.C1910_DIVISION_ID ");
		strQuery.append(" AND T704.C101_PARTY_ID = T101.C101_PARTY_ID ");
		strQuery.append(" AND t901.c901_code_id(+) = t5001.c901_po_disc_category_type ");
		strQuery.append(" AND t901a.c901_code_id(+) = t5001.C901_COLLECTOR_ID ");
		strQuery.append(" AND t704a.c704_account_id(+) = t704.c704_account_id ");
		strQuery.append(" AND t704a.c901_attribute_type(+) = 103050");
		strQuery.append(" AND t901b.c901_code_id (+) =t704a.C704A_ATTRIBUTE_VALUE ");
		strQuery.append(" AND t501.c501_ad_id = t101a.c101_user_id ");
		strQuery.append(" AND t501.c501_vp_id = t101b.c101_user_id ");
		strQuery.append(" AND t5001.c5001_cust_po_updated_by = t101_pouptby.c101_user_id ");
		strQuery.append(" AND t501.C503_INVOICE_ID IS NULL ");
		strQuery.append(" AND T501.C501_VOID_FL IS NULL ");
		strQuery.append(" AND T5001.C5001_VOID_FL IS NULL ");
		strQuery.append(" AND T704.C704_VOID_FL IS NULL ");
		strQuery.append(" AND T703.C703_VOID_FL IS NULL ");
		strQuery.append(" AND T1910.C1910_VOID_FL IS NULL ");
		strQuery.append(" AND T101.C101_VOID_FL IS NULL ");
		strQuery.append(" AND t901.c901_void_fl(+) IS NULL ");
		strQuery.append(" AND t901a.c901_void_fl(+) IS NULL ");
		strQuery.append(" AND t901a.c901_void_fl(+) IS NULL ");
		strQuery.append(" AND t704a.c704a_void_fl(+) IS NULL ");
		strQuery.append(" AND t901b.c901_void_fl (+) IS NULL ");
		strQuery.append(" AND T5001.C901_CUST_PO_STATUS IN ( "+gmARDiscrepancyModel.getStrPOStatusList()+ ") ");
		strQuery.append(" AND NVL(T501.C901_order_type,-9999) NOT IN ");
		strQuery.append(" (SELECT t906.c906_rule_value FROM t906_rules t906  WHERE t906.c906_rule_grp_id = 'EXC_PO_ORD_TYP' AND c906_rule_id= 'PO_ORDTYPE') ");
		strQuery.append(" AND T501.C1900_COMPANY_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompanyId()) +"' ");
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrFieldSales()).equals("") & !GmCommonClass.parseZero(gmARDiscrepancyModel.getStrFieldSales()).equals("0")){
			strQuery.append(" AND T701.C701_DISTRIBUTOR_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrFieldSales()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrSalesRep()).equals("") & !GmCommonClass.parseZero(gmARDiscrepancyModel.getStrSalesRep()).equals("0")){
			strQuery.append(" AND T703.C703_SALES_REP_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrSalesRep()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDivisionId()).equals("")){
			strQuery.append(" AND T501.C1910_DIVISION_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDivisionId()) +"' ");
		}
		
		if(!strPONumber.equals("")){
            strQuery.append("  AND UPPER(t501.c501_customer_po) in ('" +strPONumber +"') ");
        }
		
		if(!strOrderId.equals("")){
			strQuery.append(" AND UPPER(T501.C501_ORDER_ID) in ('" +strOrderId +"') " );
		}
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrParentAcc()).equals("") & !GmCommonClass.parseZero(gmARDiscrepancyModel.getStrParentAcc()).equals("0")){
			strQuery.append(" AND T704.C101_PARTY_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrParentAcc()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrRepAcc()).equals("") & !GmCommonClass.parseZero(gmARDiscrepancyModel.getStrRepAcc()).equals("0")){
			strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrRepAcc()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrAccid()).equals("")){
			strQuery.append(" AND T501.C704_ACCOUNT_ID = '"+ GmCommonClass.parseNull(gmARDiscrepancyModel.getStrAccid()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDisStartDate()).equals("")){ //need change
			strQuery.append(" AND trunc(T5001.C501_PO_MOVE_DISCREPANCY_DATE) >= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDisStartDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"')");
		} 	
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDisEndDate()).equals("")){//need change
			strQuery.append(" AND trunc(T5001.C501_PO_MOVE_DISCREPANCY_DATE) <= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDisEndDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"')");
		}
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResStartDate()).equals("")){//need change
			strQuery.append(" AND T5001.C5001_RESOLVED_DISC_DATE >= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResStartDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"') ");
		}
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResEndDate()).equals("")){//need change
			strQuery.append(" AND T5001.C5001_RESOLVED_DISC_DATE <= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResEndDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"') ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrOrdStartDate()).equals("")){
			strQuery.append(" AND T501.C501_ORDER_DATE >= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrOrdStartDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"') ");
		}
		
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrOrdEndDate()).equals("")){
			strQuery.append(" AND T501.C501_ORDER_DATE <= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrOrdEndDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrDtFormat())+"') ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCategoryId()).equals("")) {
			strQuery.append(" AND t5001.C901_PO_DISC_CATEGORY_TYPE ='"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCategoryId()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResolutionId()).equals("")) {
			strQuery.append(" AND t5001.C901_PO_DISC_RESOLUTION_TYPE ='"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrResolutionId()) + "' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCollectorId()).equals("")) {
			strQuery.append(" AND nvl(t5001.C901_COLLECTOR_ID, t704a.c704a_attribute_value) ='"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCollectorId()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrADId()).equals("")) {
			strQuery.append(" AND t101a.c101_user_id ='"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrADId()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyModel.getStrVPId()).equals("")) {
			strQuery.append(" AND t101b.c101_user_id ='"+GmCommonClass.parseNull(gmARDiscrepancyModel.getStrVPId()) +"' ");
		}
		if(GmCommonClass.parseNull(gmARDiscrepancyModel.getStrReadyForResId()).equals("Y")) {
			strQuery.append(" AND t5001.c5001_po_disc_resolved_fl ='Y' ");
		}
		
		if(GmCommonClass.parseNull(gmARDiscrepancyModel.getStrHFutureRemId()).equals("Y")) {
			strQuery.append(" AND (trunc(t5001.c5001_po_disc_remaind_date) < trunc(to_date('"+formatter.format(zonedDateTime.toLocalDate())+"','"+strDtFormat+"')) OR t5001.c5001_po_disc_remaind_date is null) ");
		}
		strQuery.append(")DISCREPANCYDTLS ");
		strQuery.append(" order by strDisDate desc, strRepAccNm ");
		List<GmARDiscrepancyModel> loadDiscrepancyList = this.jdbcTemplate.query(strQuery.toString(),new GmARDiscrepancyRowMapper());
		System.out.println("Discrepancy dashboard Query=>"+strQuery.toString());
		return loadDiscrepancyList;
	
	}
	
	/**
	 * This method is used to Save/update discrepancy details
	 * @param gmARDiscrepancyModel
	 * @return gmARDiscrepancyModel
	 */
	@Override
	public Optional<GmAROrderPOModel> savePODiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		try{
			String strCompanyId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompanyId());
			String strUserId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId());
			String strPlantId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrPlantId());
			String strCompZone = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompZone());
		
			gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
			StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_po_trans.gm_upd_po_discrepency_dtl")
					.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(4,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(5,String.class,ParameterMode.IN)
		    		.setParameter(1,gmCommonClass.parseNull(gmARDiscrepancyModel.getStrPONumDtlsId()))
		    		.setParameter(2, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrCollectorId()))
				    .setParameter(3, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrCategoryId()))
				    .setParameter(4, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrResolutionId()))
				    .setParameter(5, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId()));
			
			 	storedProcedureQuery.execute();
			}catch(Exception e){
				e.printStackTrace();
			}	
		return fetchPODiscrepencyIdList(gmARDiscrepancyModel);
	}
	/**
	 * This method is used fetch discrepancy details based on PO detail Id(collector,category,resolution,remind me for right panel)
	 * @param GmARDiscrepancyModel
	 * @return GmAROrderPOModel
	 */
	public Optional<GmAROrderPOModel> fetchPODiscrepencyIdList(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception {
		int strPODtlId = Integer.parseInt(gmCommonClass.parseZero(gmARDiscrepancyModel.getStrPONumDtlsId()));
		Optional<GmAROrderPOModel> loadOrderPoDetails =  gmAROrderPORepository.findByStrOrderPODtlIdAndStrVoidFlIsNull(strPODtlId);
        return loadOrderPoDetails;
	}
	/**
	  * This method is used to save the Remind me days for discrepancy
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOModel
	  */
	public Optional<GmAROrderPOModel> saveRemindMeDiscrepencyDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		try{
			String strCompanyId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompanyId());
			String strUserId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId());
			String strPlantId = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrPlantId());
			String strCompZone = GmCommonClass.parseNull(gmARDiscrepancyModel.getStrCompZone());
		
			gmEntityManager.getConnection(strCompanyId, strPlantId, strCompZone);
			StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_po_trans.gm_upd_remind_me_date_discrepency_dtl")
					.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(4,String.class,ParameterMode.IN)
		    		.setParameter(1, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrPONumDtlsId()))
		    		.setParameter(2, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrRemindMedays()))
				    .setParameter(3, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrRemindMedate()))
				    .setParameter(4, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId()));
			
			 	storedProcedureQuery.execute();
			}catch(Exception e){
				e.printStackTrace();
			}
		return fetchPODiscrepencyIdList(gmARDiscrepancyModel);
	}
	/**
	  * This method is used to fetch the log details for Category(History Icon)
	  * @param GmARDiscrepancyModel
	  * @return GmAROrderPOLogDetails
	  */
	@SuppressWarnings(value = "unchecked")
	public List<GmARDiscrepancyModel> fetchPOLogDetails(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		String strQuery =  " SELECT  t5002.c5002_order_po_dtls_log_id strLogId,t5002.c5002_po_dtls_code_name strLogCodeNm,GET_USER_NAME(t5002.c5002_last_updated_by) strLogUpdateBy,TO_CHAR(t5002.C5002_LAST_DATED_DATE,'"+gmARDiscrepancyModel.getStrDtFormat()+"') strLogUpdateDate"
				         + " FROM t5002_order_po_dtls_log t5002 WHERE t5002.c5001_order_po_dtls_id = '"+gmARDiscrepancyModel.getStrPONumDtlsId()+"'"
				         + " AND t5002.c901_po_history_type = '"+gmARDiscrepancyModel.getStrPODtlHisType()+"' AND t5002.c5002_void_fl IS NULL ORDER BY t5002.c5002_order_po_dtls_log_id DESC";

    List<GmARDiscrepancyModel> fetchPOLogDtlList = this.jdbcTemplate.query(strQuery,new GmARPOLogRowMapper());
		return fetchPOLogDtlList;
	}
	
	/**
	 * This method is used to fetch line item discrepancy details
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 * @exception Exception 
	 */
	public List<GmARPODiscrepancyLineItemModel> fetchDiscrepancyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception
	{
		StringBuilder strQuery = new StringBuilder();
		
		strQuery.append(" SELECT t501.C501_ORDER_ID strOrderid, t502.C205_PART_NUMBER_ID strPnumId, SUM (t502.C502_ITEM_QTY) strItemQty, ");
		strQuery.append(" t502.C502_ITEM_PRICE strItemPrice,(t502.c502_item_price) * SUM(t502.c502_item_qty) strExtDoAmt, t5004.C205_PO_PART_NUMBER_ID strPOPartNum, SUM(t5004.C5004_PO_QTY) strPOPartQty, ");
		strQuery.append(" t5004.C5004_PO_PRICE strPOPrice, nvl(t5001.C5001_ORDER_PO_DTLS_ID,0) strPODtlsId, nvl(t5004.C5004_ORDER_PO_LINE_ITEM_DTLS,0) strPOLIDtlsId,");
		strQuery.append(" t5004.c5004_po_price * SUM(t5004.c5004_po_qty) strExtPoAmt, t205.C205_PART_NUM_DESC strPartDesc,t901.c901_code_nm strDescType,  t901a.c901_code_nm strStatus,t901.c901_code_id strDescTypeId, t901a.c901_code_id strStatusId ");
		strQuery.append(" FROM T5004_ORDER_PO_LINE_ITEM_DTLS t5004, T502_ITEM_ORDER t502, T5001_ORDER_PO_DTLS t5001, T501_ORDER t501, t205_part_number t205 , t901_code_lookup t901,  t901_code_lookup t901a");
		strQuery.append(" WHERE t5001.C5001_ORDER_PO_DTLS_ID = t5004.C5001_ORDER_PO_DTLS_ID (+) ");
		strQuery.append("  AND t5001.C5001_CUST_PO_ID = t501.C501_CUSTOMER_PO ");
		strQuery.append("  AND (t501.C501_PARENT_ORDER_ID = t5001.C501_ORDER_ID OR t501.C501_ORDER_ID = t5001.C501_ORDER_ID) ");
		strQuery.append(" AND t501.C501_ORDER_ID = t502.C501_ORDER_ID ");
		strQuery.append(" AND t502.C205_PART_NUMBER_ID = t5004.C205_PO_PART_NUMBER_ID (+) ");
		strQuery.append(" AND  t502.c205_part_number_id = t205.c205_part_number_id");
		strQuery.append(" AND t901.c901_code_id (+) = t5004.c901_po_discrepancy_type");
		strQuery.append(" AND t901a.c901_code_id (+) = t5004.c901_po_resolution");
		strQuery.append(" AND t501.C501_ORDER_ID IN( ");
		strQuery.append(" SELECT c501_order_id FROM t501_order ");
		strQuery.append(" WHERE c704_account_id = '" +gmARDiscrepancyModel.getStrAccId()+"'"); 
		strQuery.append(" AND C501_CUSTOMER_PO = '" +gmARDiscrepancyModel.getStrPONumber()+"'");
		strQuery.append(" AND NVL (C901_order_type, - 9999)     <> 2535 ");  // 2535 - Sales discount
		strQuery.append(" AND NVL (c901_order_type, - 9999) NOT IN( ");
		strQuery.append(" SELECT t906.c906_rule_value FROM t906_rules t906 ");
		strQuery.append(" WHERE t906.c906_rule_grp_id = 'EXC_PO_ORD_TYP' ");
		strQuery.append(" AND t906.c906_rule_id = 'PO_ORDTYPE') ");
		strQuery.append(" AND c501_void_fl IS NULL) ");
		strQuery.append(" AND t5001.C901_CUST_PO_STATUS = 109544 "); // 109544 - Move to discrepancy  
		strQuery.append(" AND t5001.C5001_CUST_PO_ID  = '"+gmARDiscrepancyModel.getStrPONumber()+"'"); 
		strQuery.append(" AND t5001.c704_account_id  = '"+gmARDiscrepancyModel.getStrAccId()+"'"); 
		strQuery.append(" AND t5004.c205_do_part_number_id(+) IS NOT NULL ");
		strQuery.append(" AND t5004.C5004_VOID_FL  IS NULL ");
		strQuery.append(" AND t5001.C5001_VOID_FL  IS NULL");
		strQuery.append(" GROUP BY t501.C501_ORDER_ID, t502.C205_PART_NUMBER_ID, t502.C502_ITEM_PRICE,t205.c205_part_num_desc, ");
		strQuery.append(" t5004.C205_PO_PART_NUMBER_ID, t5004.C5004_PO_PRICE, t5001.C5001_ORDER_PO_DTLS_ID,t5004.c5004_order_po_line_item_dtls,t901.c901_code_nm,t901a.c901_code_nm,t901.c901_code_id, t901a.c901_code_id ");
		strQuery.append(" UNION ALL SELECT NULL strOrderid, NULL strPnumId, NULL strItemQty, NULL strItemPrice,NULL strExtDoAmt, t5004.C205_PO_PART_NUMBER_ID strPOPartNum, SUM(t5004.C5004_PO_QTY) strPOPartQty, ");
		strQuery.append(" t5004.C5004_PO_PRICE strPOPrice, nvl(t5001.C5001_ORDER_PO_DTLS_ID,0) strPODtlsId, nvl(t5004.C5004_ORDER_PO_LINE_ITEM_DTLS,0) strPOLIDtlsId,");
		strQuery.append(" t5004.c5004_po_price * SUM(t5004.c5004_po_qty) strExtPoAmt, NULL strPartDesc,t901.c901_code_nm strDescType,  t901a.c901_code_nm strStatus,t901.c901_code_id strDescTypeId, t901a.c901_code_id strStatusId ");
		strQuery.append(" FROM T5004_ORDER_PO_LINE_ITEM_DTLS t5004, T5001_ORDER_PO_DTLS t5001 , t901_code_lookup t901,  t901_code_lookup t901a");
		strQuery.append(" WHERE t5001.c5001_order_po_dtls_id = t5004.c5001_order_po_dtls_id ");
		strQuery.append(" AND t5001.c901_cust_po_status = 109544 ");
		strQuery.append(" AND t5001.C5001_CUST_PO_ID  = '"+gmARDiscrepancyModel.getStrPONumber()+"'"); 
		strQuery.append(" AND t5001.c704_account_id  = '"+gmARDiscrepancyModel.getStrAccId()+"'"); 
		strQuery.append(" AND t901.c901_code_id (+) = t5004.c901_po_discrepancy_type");
		strQuery.append(" AND t901a.c901_code_id (+) = t5004.c901_po_resolution");
		strQuery.append(" AND t5004.c205_do_part_number_id IS NULL");
		strQuery.append(" AND t5004.c5004_void_fl (+) IS NULL ");
		strQuery.append(" AND t5001.c5001_void_fl IS NULL ");
		strQuery.append(" GROUP BY t5004.C205_PO_PART_NUMBER_ID, t5004.C5004_PO_PRICE, t5001.C5001_ORDER_PO_DTLS_ID, ");
		strQuery.append(" t5004.c5004_order_po_line_item_dtls,t901.c901_code_nm,t901a.c901_code_nm,t901.c901_code_id, t901a.c901_code_id ");
		strQuery.append(" ORDER BY 1,2 ");
		
		List<GmARPODiscrepancyLineItemModel> loadPOLineItemList = this.jdbcTemplate.query(strQuery.toString(),new GmARPODiscrepancyLineItemRowMapper());
		System.out.println("Line Item Fetch Query=>"+strQuery.toString());
		return loadPOLineItemList;
	}
	
	
	/**
	 * This method is used to save/update line item discrepancy details for selected PO
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public String saveDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		
		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_po_trans.gm_save_po_discrepency_line_item")
				.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(3,String.class,ParameterMode.IN)
	    		.registerStoredProcedureParameter(4,String.class,ParameterMode.OUT)
	    		.setParameter(1, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrPOLineItemDtls()))
			    .setParameter(2, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrPnumInputStr()))
		        .setParameter(3, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId()));
		
		storedProcedureQuery.execute();
        String strMsg = (String) GmCommonClass.parseNull(storedProcedureQuery.getOutputParameterValue(4));
		
		return strMsg;
		 	
	}
	
	/**
	 * This method is used to save/update line item discrepancy details for selected PO
	 * @param GmARDiscrepancyModel
	 * @return GmARDiscrepancyModel
	 */
	public void removeDiscrepencyLineItems(GmARDiscrepancyModel gmARDiscrepancyModel) throws Exception{
		try{
			StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("gm_pkg_ar_po_trans.gm_remove_po_line_item_dtls")
					.registerStoredProcedureParameter(1,String.class,ParameterMode.IN)
		    		.registerStoredProcedureParameter(2,String.class,ParameterMode.IN)
		    		.setParameter(1, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrPOLineItemDtls()))
				    .setParameter(2, gmCommonClass.parseNull(gmARDiscrepancyModel.getStrUserId()));
			
			 	storedProcedureQuery.execute();
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	
	
	/**
	 * This method is used to fetch discrepancy details report
	 * @param gmARDiscrepancyDtlReportModel
	 * @return gmARDiscrepancyDtlReportModel
	 * @exception Exception 
	 */
	public List<GmARDiscrepancyDtlReportModel> loadDiscrepancyDtlReport(GmARDiscrepancyDtlReportModel gmARDiscrepancyDtlReportModel) throws Exception{

		String strPONumber = GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCustPONumber());        
		if(!strPONumber.equals("")){
			strPONumber = strPONumber.replaceAll(",", "','");
		}
		strPONumber =strPONumber.toUpperCase();


		String strOrderId = GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrOrderId());      
		if(!strOrderId.equals("")){
			strOrderId = strOrderId.replaceAll(",", "','");
		}
		strOrderId =strOrderId.toUpperCase();
		
		StringBuilder strQuery = new StringBuilder();

		strQuery.append(" SELECT  t501.c501_order_id strOrderid, t501.c501_customer_po strPONumber, t704.c704_account_nm strRepAcc, ");
		strQuery.append(" t703.c703_sales_rep_name strSalesRep, t205_PO.c205_part_num_desc strPartDesc, t5004.c205_do_part_number_id strDOPartnum, ");
		strQuery.append(" sum(t5004.c5004_do_qty) strDOPartQty, t5004.c5004_do_price strDOItemPrice, t5004.c205_po_part_number_id strPOPartNum, ");
		strQuery.append(" sum(t5004.c5004_po_qty) strPOPartQty, t5004.c5004_po_price strPOItemPrice, (t5004.c5004_do_price * sum(t5004.c5004_do_qty)) strExtDoAmt, ");
		strQuery.append(" (t5004.c5004_po_price * sum(t5004.c5004_po_qty)) strExtPoAmt, nvl(t901_col.c901_code_nm, t901_acat.c901_code_nm) strCollectorNm, t901_cat.c901_code_nm strCategoryNm, ");
		strQuery.append(" t901_dtype.c901_code_nm strDiscType, t901_dsts.c901_code_nm strDiscStatus, TO_CHAR (T5001.c5001_resolved_disc_date,'"+gmARDiscrepancyDtlReportModel.getStrDtFormat()+"') strResDate, ");
		strQuery.append(" TO_CHAR (T5001.c501_po_move_discrepancy_date,'"+gmARDiscrepancyDtlReportModel.getStrDtFormat()+"') strDisDate ");
		strQuery.append(" FROM t5001_order_po_dtls  t5001, t5004_order_po_line_item_dtls t5004, t501_order t501, t205_part_number t205_DO, t205_part_number t205_PO, ");
		strQuery.append(" t704_account t704,t701_distributor t701,t703_sales_rep t703,t1910_division t1910, t901_code_lookup  t901_col, t901_code_lookup  t901_cat, t901_code_lookup t901_acat, ");
		strQuery.append(" t901_code_lookup t901_dtype,  t901_code_lookup t901_dsts, t101_party t101, t704a_account_attribute t704a");
		strQuery.append(" WHERE t5001.c5001_order_po_dtls_id = t5004.c5001_order_po_dtls_id ");
		strQuery.append(" AND t501.c501_order_id = t5001.c501_order_id ");
		strQuery.append(" AND t501.c704_account_id = t704.c704_account_id ");
		strQuery.append(" AND t703.c701_distributor_id = t701.C701_DISTRIBUTOR_ID ");
		strQuery.append(" AND t501.c703_sales_rep_id = t703.c703_sales_rep_id ");
		strQuery.append(" AND t501.c1910_division_id = t1910.c1910_division_id ");
		strQuery.append(" AND t704.c101_party_id = t101.c101_party_id ");
		strQuery.append(" AND t205_PO.c205_part_number_id = t5004.c205_po_part_number_id ");
		strQuery.append(" AND t205_DO.c205_part_number_id(+) = t5004.c205_do_part_number_id ");
		strQuery.append(" AND t901_cat.c901_code_id(+) = t5001.c901_po_disc_category_type ");
		strQuery.append(" AND t901_col.c901_code_id(+) = t5001.c901_collector_id ");
		strQuery.append(" AND t901_dtype.c901_code_id(+) = t5004.c901_po_discrepancy_type ");
		strQuery.append(" AND t901_dsts.c901_code_id(+) = t5004.c901_po_resolution ");
		strQuery.append(" AND t704a.c704_account_id(+) = t704.c704_account_id ");
		strQuery.append(" AND t704a.c901_attribute_type(+) = 103050 "); // 103050 - Collector
		strQuery.append(" AND t901_acat.c901_code_id(+)  = t704a.C704A_ATTRIBUTE_VALUE ");
		strQuery.append(" AND T501.c501_void_fl IS NULL ");
		strQuery.append(" AND T5001.c5001_void_fl IS NULL ");
		strQuery.append(" AND T704.c704_void_fl IS NULL ");
		strQuery.append(" AND T703.c703_void_fl IS NULL ");
		strQuery.append(" AND t1910.c1910_void_fl IS NULL ");
		strQuery.append(" AND t5004.c5004_void_fl  IS NULL ");
		strQuery.append(" AND t704a.c704a_void_fl(+) IS NULL ");
		strQuery.append(" AND NVL (C901_order_type, - 9999)     <> 2535 ");
		strQuery.append(" AND NVL(T501.C901_order_type,-9999) NOT IN ");
		strQuery.append(" (SELECT t906.c906_rule_value FROM t906_rules t906  WHERE t906.c906_rule_grp_id = 'EXC_PO_ORD_TYP' AND c906_rule_id= 'PO_ORDTYPE' AND c501_void_fl IS NULL)");
		strQuery.append(" AND t501.c1900_company_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCompanyId()) +"' ");

		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrFieldSales()).equals("")){
			strQuery.append(" AND T701.c701_distributor_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrFieldSales()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrSalesRep()).equals("")){
			strQuery.append(" AND T703.c703_sales_rep_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrSalesRep()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDivisionId()).equals("")){
			strQuery.append(" AND T501.c1910_division_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDivisionId()) +"' ");
		}

		if(!strPONumber.equals("")){
			strQuery.append("  AND UPPER(t501.c501_customer_po) in ('" +strPONumber +"') ");
		}

		if(!strOrderId.equals("")){
			strQuery.append(" AND UPPER(t501.C501_ORDER_ID) in ('" +strOrderId +"') " );
		}

		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrParentAcc()).equals("")){
			strQuery.append(" AND t704.c101_party_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrParentAcc()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrRepAcc()).equals("")){
			strQuery.append(" AND t501.c704_account_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrRepAcc()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrAccid()).equals("")){
			strQuery.append(" AND t501.c704_account_id = '"+ GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrAccid()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDisStartDate()).equals("")){
			strQuery.append(" AND trunc(t5001.c501_po_move_discrepancy_date) >= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDisStartDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDtFormat())+"')");
		} 	

		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDisEndDate()).equals("")){
			strQuery.append(" AND trunc(t5001.C501_PO_MOVE_DISCREPANCY_DATE) <= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDisEndDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDtFormat())+"')");
		}

		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResStartDate()).equals("")){
			strQuery.append(" AND trunc(t5001.c5001_resolved_disc_date) >= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResStartDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDtFormat())+"') ");
		}

		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResEndDate()).equals("")){
			strQuery.append(" AND trunc(t5001.c5001_resolved_disc_date) <= to_date('"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResEndDate())+"','"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrDtFormat())+"') ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCategoryId()).equals("")) {
			strQuery.append(" AND t5001.c901_po_disc_category_type ='"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCategoryId()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResolutionId()).equals("")) {
			strQuery.append(" AND t5001.c901_po_disc_resolution_type ='"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrResolutionId()) + "' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCollectorId()).equals("")) {
			strQuery.append(" AND nvl(t5001.C901_COLLECTOR_ID, t704a.c704a_attribute_value) ='"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrCollectorId()) +"' ");
		}
		if(!GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrStatusId()).equals("")) {
			strQuery.append(" AND t5004.c901_po_resolution ='"+GmCommonClass.parseNull(gmARDiscrepancyDtlReportModel.getStrStatusId()) +"' ");
		}

		strQuery.append(" GROUP BY t501.c501_order_id, t501.c501_customer_po, t704.c704_account_nm, t703.c703_sales_rep_name, t205_PO.c205_part_num_desc,");
		strQuery.append(" t5004.c205_do_part_number_id, t5004.c5004_do_price, t5004.c205_po_part_number_id, t5004.c5004_po_price, ");
		strQuery.append(" t901_dsts.c901_code_nm, t901_cat.c901_code_nm, t901_col.c901_code_nm, t901_dtype.c901_code_nm, t5001.c501_po_move_discrepancy_date, t5001.c5001_resolved_disc_date, t901_acat.c901_code_nm ");
		strQuery.append(" order by t5001.c501_po_move_discrepancy_date desc, t704.c704_account_nm ");


		List<GmARDiscrepancyDtlReportModel> loadDiscrepancyDtlReport = this.jdbcTemplate.query(strQuery.toString(),new GmARPODiscrepancyDtlReportRowMapper());
		System.out.println("Detail Reprt Fetch Query=>"+strQuery.toString());
		return loadDiscrepancyDtlReport;

	}

}
