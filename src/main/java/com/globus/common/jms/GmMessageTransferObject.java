package com.globus.common.jms;

import java.io.Serializable;


@SuppressWarnings("serial")
public class GmMessageTransferObject implements Serializable{
	private Object messageObject ;
	private String consumerClass ;
	
	
	public String getConsumerClass() {
		return consumerClass;
	}
	public void setConsumerClass(String consumerClass) {
		this.consumerClass = consumerClass;
	}
	public Object getMessageObject() {
		return messageObject;
	}
	public void setMessageObject(Object messageObject) {
		this.messageObject = messageObject;
	}
	
	@Override
	public String toString() {
		return this.consumerClass + this.messageObject.toString();
	}
	
}