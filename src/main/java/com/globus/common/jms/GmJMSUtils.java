package com.globus.common.jms;

import java.util.HashMap;


/**
 * This GmJMSUtils class used to call the GmARBatchConsumerMDB class in spine it using jms
 * 
 */
public class GmJMSUtils {

	/**
	 * This method is call the GmARBatchConsumerMDB class in spineit using jms
	 * 
	 * @param hmParam,strConsumerClass
	 * @exception Exception        
	 */
	public void sendMessage(HashMap hmParam, String strQueueName, String strConsumerClass)
			throws Exception {
		GmQueueProducer qprod = new GmQueueProducer();
		GmMessageTransferObject tf = new GmMessageTransferObject();
		tf.setMessageObject(hmParam);
		tf.setConsumerClass(strConsumerClass);
		qprod.sendMessage(hmParam, strQueueName);
	}
}
