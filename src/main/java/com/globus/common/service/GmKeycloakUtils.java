package com.globus.common.service;

import com.globus.common.model.GmKeycloakUser;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class GmKeycloakUtils {
	public String getUsername() throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = ((KeycloakAuthenticationToken) authentication).getAccount().getPrincipal().getName();
		return username;
	}

	public Set<String> getRoles() throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return ((KeycloakAuthenticationToken) authentication).getAccount().getRoles();
	}

	public GmKeycloakUser getGmKeycloakUser() throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		KeycloakAuthenticationToken keycloakToken = ((KeycloakAuthenticationToken) authentication);
		String username = keycloakToken.getAccount().getPrincipal().getName();
		Set<String> roles = keycloakToken.getAccount().getRoles();

		GmKeycloakUser gmKeycloakUser = new GmKeycloakUser();
		gmKeycloakUser.setUsername(username);
		gmKeycloakUser.setRoles(roles);

		return gmKeycloakUser;
	}
}
