package com.globus.common.exception;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * This GmExceptionHandler class implements an application that using to Handle
 * all exception in entire application
 * 
 * @author Felsia T
 * @since 2020-02-27
 */
@ControllerAdvice
public class GmExceptionhandler extends ResponseEntityExceptionHandler {

	private final static Logger gmLogger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleEntityNotFound(Exception ex) {
		GmErrorResponse apiError = new GmErrorResponse(HttpStatus.NOT_FOUND);
		gmLogger.info(ex.getMessage());
		apiError.setMessage(ex.getMessage());
		return buildResponseEntity(apiError);
	}

	private ResponseEntity<Object> buildResponseEntity(GmErrorResponse apiError) {
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}
}
