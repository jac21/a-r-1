package com.globus.common.dao;

import com.globus.common.model.GmUser;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GmUserRowMapper implements RowMapper<GmUser> {
	@Override
	public GmUser mapRow(ResultSet resultSet, int i) throws SQLException {
		GmUser gmuser = new GmUser();
		gmuser.setLoginname(resultSet.getString(1));
		gmuser.setPartyid(resultSet.getString(2));
		gmuser.setUserid(resultSet.getString(3));
		gmuser.setUsername(resultSet.getString(4));
		return gmuser;
	}
}
