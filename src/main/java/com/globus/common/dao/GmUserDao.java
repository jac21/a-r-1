package com.globus.common.dao;

import com.globus.common.model.GmUser;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GmUserDao {
	private JdbcTemplate jdbcTemplate;

	public GmUserDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public GmUser getUserInformation(String username) throws Exception {
		StringBuilder builder = new StringBuilder("SELECT t102.c102_login_username loginname, "
				+ "  t101.c101_party_id partyid, " + "  t101.c101_user_id userid, " + "  t101.c101_user_f_name "
				+ "  ||' ' " + "  || t101.c101_user_l_name username  FROM t102_user_login t102, " + "  t101_user t101 "
				+ " WHERE c102_login_username = '" + username + "' " + "AND t101.c101_user_id =t102.c101_user_id "
				+ "and t101.c901_user_status = 311");
		List<GmUser> gmUsers = this.jdbcTemplate.query(builder.toString(), new GmUserRowMapper());
		if (gmUsers.size() > 0)
			return gmUsers.get(0);
		else
			return new GmUser();
	}
	
}
