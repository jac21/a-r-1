var urlIndex = "https://erppreprod.spineit.net/erpar/";
var urlMicroapp = "https://erppreprod.spineit.net/erpmicroappcommon/";
var url = urlIndex+"api/";
var microappUrl = urlMicroapp+"api/";
var microappUrlFile = urlMicroapp + "apiFiles/";
var varJsonURL = urlIndex+"dist/app/GmMessage.json";
var accessTokenLifespanInMinutes = 5; // In Minutes accessTokenLifespanInMinutes
var fileUploadPath = "////sw10erp05-p//spineitfiles//Appnode//do_document//";
/* Keycloak Values*/
var keycloakConfig = { url: "https://loginpreprod.spineit.net/auth/", realm: "globus-medical", clientId: "AR-Client"};
