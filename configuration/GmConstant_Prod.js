var urlIndex = "https://erp.spineit.net/erpar/";
var urlMicroapp = "https://erp.spineit.net/erpmicroappcommon/";
var url = urlIndex+"api/";
var microappUrl = urlMicroapp+"api/";
var microappUrlFile = urlMicroapp + "apiFiles/";
var varJsonURL = urlIndex+"dist/app/GmMessage.json";
var accessTokenLifespanInMinutes = 5; // In Minutes accessTokenLifespanInMinutes
var fileUploadPath = "////SW10WEB02//spineitfiles//Appnode//do_document//";
/* Keycloak Values*/
var keycloakConfig = { url: "https://login.spineit.net/auth/", realm: "globus-medical", clientId: "AR-Client" };